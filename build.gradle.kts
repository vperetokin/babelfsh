import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.9.21"
    antlr
    id("dev.jacomet.logging-capabilities") version "0.11.0"
    kotlin("plugin.serialization") version "1.9.21"
}

group = "de.mii-termserv"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.jetbrains.kotlin:kotlin-test")
}

tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(21)
}

dependencies {
    val antlrVersion = "4.13.1"
    val hapiFhirVersion = "6.10.4"
    val kotestVersion = "5.8.1"
    antlr("org.antlr:antlr4:$antlrVersion")
    implementation("org.antlr:antlr4-runtime:$antlrVersion")
    implementation("com.xenomachina:kotlin-argparser:2.0.7")
    implementation("ca.uhn.hapi.fhir:hapi-fhir-base:$hapiFhirVersion")
    implementation("ca.uhn.hapi.fhir:hapi-fhir-structures-r4b:$hapiFhirVersion")
    implementation("ca.uhn.hapi.fhir:hapi-fhir-structures-r5:$hapiFhirVersion")
    implementation("ca.uhn.hapi.fhir:hapi-fhir-converter:$hapiFhirVersion")
    implementation("com.github.doyaaaaaken:kotlin-csv-jvm:1.9.3")
    implementation("org.slf4j:slf4j-api:2.0.12")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.2")
    implementation("org.apache.logging.log4j:log4j-api:2.22.1")
    implementation("org.apache.logging.log4j:log4j-core:2.22.1")
    implementation("org.apache.logging.log4j:log4j-slf4j2-impl:2.22.1")
    implementation("org.apache.logging.log4j:log4j-api-kotlin:1.4.0")
    implementation("jakarta.xml.bind:jakarta.xml.bind-api:4.0.2")
    implementation("com.sun.xml.bind:jaxb-impl:4.0.5")
    testImplementation("io.kotest:kotest-runner-junit5:$kotestVersion")
    testImplementation("io.kotest:kotest-assertions-core:$kotestVersion")
    testImplementation("io.kotest:kotest-property:$kotestVersion")
}

tasks.withType<KotlinCompile>().configureEach {
    dependsOn(tasks.withType<AntlrTask>())
}

tasks.withType<Test>().configureEach {
    useJUnitPlatform()
}

sourceSets {
    main {
        antlr {
            srcDir("src/main/antlr")
        }
    }
}

tasks.generateGrammarSource {
    arguments = arguments + listOf("-visitor", "-package", "de.mii_termserv.babelfsh.antlr")
    outputDirectory = file("${projectDir}/src/main/java/de/mii_termserv/babelfsh/antlr")
}

loggingCapabilities {
    enforceLog4J2()
}
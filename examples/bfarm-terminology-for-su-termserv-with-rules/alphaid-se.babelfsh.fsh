RuleSet: alphaid-se-metadata
* ^url = "http://fhir.de/CodeSystem/bfarm/alpha-id"
* insert bfarm-sutermserv-tags
* ^meta.profile = "http://hl7.org/fhir/StructureDefinition/shareablecodesystem"
* ^copyright = "Die Erstellung erfolgt unter Verwendung der maschinenlesbaren Fassung des Bundesinstituts für Arzneimittel und Medizinprodukte (BfArM)."
* ^status = #active
* ^experimental = false
* ^publisher = "Bundesinstitut für Arzneimittel und Medizinprodukte (BfArM)"
* ^property[+].code = #icd_10_primaer
* ^property[=].type = #string
* ^property[=].description = "Der ICD-Code aus der Alpha-ID"
* ^property[+].code = #icd_10_stern
* ^property[=].type  = #string
* ^property[=].description = "Der Sterncode aus der Alpha-ID"
* ^property[+].code = #icd_10_zusatz
* ^property[=].type = #string
* ^property[=].description = "Der Sekundär-Code (mit Ausrufezeichen) aus der Alpha-ID"
* ^property[+].code = #orpha
* ^property[=].type = #string
* ^property[=].description = "Der mit dem Konzept verknüpfte Orphanet-Code"
* ^property[=].uri = "http://www.orpha.net"
* ^property[+].code = #inactive
* ^property[=].type = #boolean
* ^property[=].description = "Gibt an, ob der Eintrag gültig ist"
* ^property[=].uri = "http://hl7.org/fhir/concept-properties#inactive"

RuleSet: alphaid-se-babelfsh-2018ff(version, path, oid)
* ^version = "{version}"
* ^identifier.system = "urn:ietf:rfc:3986"
* ^identifier.value = "urn:oid:{oid}"
* insert alphaid-se-metadata
* ^property[+].code = #icd_10_primaer2
* ^property[=].type = #string
* ^property[=].description = "Der zweite Primär-Code aus der Alpha-ID"
/*^babelfsh
csv --path='{path}'
--headers=["gueltig", "code", "icd_10", "stern", "ausrufezeichen", "icd_10_2", "orpha", "display"]
--delimiter='|'
--charset=UTF-8
--code-column=code
--display-column='display'
--property-mapping=[{"column":"icd_10","property":"icd_10_primaer"},{"column":"stern","property":"icd_10_stern"},{"column":"ausrufezeichen","property":"icd_10_zusatz"},{"column":"icd_10_2","property":"icd_10_primaer2"},{"column":"orpha","property":"orpha"},{"column":"gueltig","property":"inactive","mapper":{"id":"boolean","arguments":{"true":"0","false":"1"}}}]
^babelfsh*/

// The 2015-2017 version of the Alpha-ID-SE does not contain a second primary code.
// Therefore, the property for the second primary code is not added to the metadata, and the invocation for the CSV transformation does not contain the column for the second primary code.
// 2017 is special since the encoding is different from 2015 and 2016, but the same columns are present as with 2015 and 2016.
RuleSet: alphaid-se-babelfsh_2015-2016(version, path, oid)
* ^version = "{version}"
* ^identifier.system = "urn:ietf:rfc:3986"
* ^identifier.value = "urn:oid:{oid}"
* insert alphaid-se-metadata
/*^babelfsh
csv --path='{path}'
--headers=["gueltig", "code", "icd_10", "stern", "ausrufezeichen", "orpha", "display"]
--delimiter='|'
--charset=ISO-8859-1
--code-column=code
--display-column='display'
--property-mapping=[{"column":"icd_10","property":"icd_10_primaer"},{"column":"stern","property":"icd_10_stern"},{"column":"ausrufezeichen","property":"icd_10_zusatz"},{"column":"orpha","property":"orpha"},{"column":"gueltig","property":"inactive","mapper":{"id":"boolean","arguments":{"true":"0","false":"1"}}}]
^babelfsh*/

RuleSet: alphaid-se-babelfsh_2017(version, path, oid)
* ^version = "{version}"
* ^identifier.system = "urn:ietf:rfc:3986"
* ^identifier.value = "urn:oid:{oid}"
* insert alphaid-se-metadata
/*^babelfsh
csv --path='{path}'
--headers=["gueltig", "code", "icd_10", "stern", "ausrufezeichen", "orpha", "display"]
--delimiter='|'
--charset=UTF-8
--code-column=code
--display-column='display'
--property-mapping=[{"column":"icd_10","property":"icd_10_primaer"},{"column":"stern","property":"icd_10_stern"},{"column":"ausrufezeichen","property":"icd_10_zusatz"},{"column":"orpha","property":"orpha"},{"column":"gueltig","property":"inactive","mapper":{"id":"boolean","arguments":{"true":"0","false":"1"}}}]
^babelfsh*/

CodeSystem: AlphaIdSe
Id: alphaid-se-2015
Title: "Alpha-ID-SE"
Description: "The Alpha-ID is a sequential and stable identification number, which is allocated to each entry in the alphabetical index. It permits the encoding of medical and natural language diagnostic terms."
* insert alphaid-se-babelfsh_2015-2016("2015", "./input-files/icd10gm2015_alphaidse_edvtxt_20140930.txt", "1.2.276.0.76.5.428")

CodeSystem: AlphaIdSe
Id: alphaid-se-2016
Title: "Alpha-ID-SE"
Description: "The Alpha-ID is a sequential and stable identification number, which is allocated to each entry in the alphabetical index. It permits the encoding of medical and natural language diagnostic terms."
* insert alphaid-se-babelfsh_2015-2016("2016", "./input-files/icd10gm2016_alphaidse_edvtxt_20151002.txt", "1.2.276.0.76.5.433")

CodeSystem: AlphaIdSe
Id: alphaid-se-2017
Title: "Alpha-ID-SE"
Description: "The Alpha-ID is a sequential and stable identification number, which is allocated to each entry in the alphabetical index. It permits the encoding of medical and natural language diagnostic terms."
* insert alphaid-se-babelfsh_2017("2017", "./input-files/icd10gm2017_alphaidse_edvtxt_20161005.txt", "1.2.276.0.76.5.466")

CodeSystem: AlphaIdSe
Id: alphaid-se-2018
Title: "Alpha-ID-SE"
Description: "The Alpha-ID is a sequential and stable identification number, which is allocated to each entry in the alphabetical index. It permits the encoding of medical and natural language diagnostic terms."
* insert alphaid-se-babelfsh-2018ff("2018", "./input-files/icd10gm2018_alphaidse_edvtxt_20171004.txt", "1.2.276.0.76.5.474")

CodeSystem: AlphaIdSe
Id: alphaid-se-2019
Title: "Alpha-ID-SE"
Description: "The Alpha-ID is a sequential and stable identification number, which is allocated to each entry in the alphabetical index. It permits the encoding of medical and natural language diagnostic terms."
* insert alphaid-se-babelfsh-2018ff("2019", "./input-files/icd10gm2019_alphaidse_edvtxt_20181005.txt", "1.2.276.0.76.5.480")

CodeSystem: AlphaIdSe
Id: alphaid-se-2020
Title: "Alpha-ID-SE"
Description: "The Alpha-ID is a sequential and stable identification number, which is allocated to each entry in the alphabetical index. It permits the encoding of medical and natural language diagnostic terms."
* insert alphaid-se-babelfsh-2018ff("2020", "./input-files/icd102020_alphaidse_edvtxt_20191004.txt", "1.2.276.0.76.5.489")

CodeSystem: AlphaIdSe
Id: alphaid-se-2021
Title: "Alpha-ID-SE"
Description: "The Alpha-ID is a sequential and stable identification number, which is allocated to each entry in the alphabetical index. It permits the encoding of medical and natural language diagnostic terms."
* insert alphaid-se-babelfsh-2018ff("2021", "./input-files/icd10gm2021_alphaidse_edvtxt_20201002.txt", "1.2.276.0.76.5.505")

CodeSystem: AlphaIdSe
Id: alphaid-se-2022
Title: "Alpha-ID-SE"
Description: "The Alpha-ID is a sequential and stable identification number, which is allocated to each entry in the alphabetical index. It permits the encoding of medical and natural language diagnostic terms."
* insert alphaid-se-babelfsh-2018ff("2022", "./input-files/icd10gm2022_alphaidse_edvtxt_20211001_20220114.txt", "1.2.276.0.76.5.521")

CodeSystem: AlphaIdSe
Id: alphaid-se-2023
Title: "Alpha-ID-SE"
Description: "The Alpha-ID is a sequential and stable identification number, which is allocated to each entry in the alphabetical index. It permits the encoding of medical and natural language diagnostic terms."
* insert alphaid-se-babelfsh-2018ff("2023", "./input-files/icd10gm2023_alphaidse_edvtxt_20220930.txt", "1.2.276.0.76.5.532")

CodeSystem: AlphaIdSe
Id: alphaid-se-2024
Title: "Alpha-ID-SE"
Description: "The Alpha-ID is a sequential and stable identification number, which is allocated to each entry in the alphabetical index. It permits the encoding of medical and natural language diagnostic terms."
* insert alphaid-se-babelfsh-2018ff("2024", "./input-files/icd10gm2024_alphaidse_edvtxt_20230929.txt", "1.2.276.0.76.5.538")
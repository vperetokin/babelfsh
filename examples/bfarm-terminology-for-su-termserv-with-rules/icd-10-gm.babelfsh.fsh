RuleSet: icd10gm-metadata(version)
* insert bfarm-sutermserv-tags
* ^meta.profile = "http://hl7.org/fhir/StructureDefinition/shareablecodesystem"
* ^url = "http://fhir.de/CodeSystem/bfarm/icd-10-gm"
* ^valueSet = "http://fhir.de/ValueSet/bfarm/icd-10-gm"
* ^version = "{version}"
* ^copyright = "Die Erstellung erfolgt unter Verwendung der maschinenlesbaren Fassung des Bundesinstituts für Arzneimittel und Medizinprodukte (BfArM)."
* ^status = #active
* ^experimental = false
* ^property[+].code = #kind
* ^property[=].description = "Art der Schlüsselnummer, eines von: [chapter, block, category]"
* ^property[=].type = #string
* ^property[+].code = #inclusion
* ^property[=].description = "Ein Inklusisionskriterium"
* ^property[=].type = #string
* ^property[+].code = #exclusion
* ^property[=].description = "Ein Exklusionskriterium"
* ^property[=].type = #string
* ^property[+].code = #definition
* ^property[=].description = "Die Definition des Codes"
* ^property[=].type = #string
* ^property[+].code = #introduction
* ^property[=].description = "Die Einführung eines Kapitels"
* ^property[=].type = #string
* ^property[+].code = #note
* ^property[=].description = "Eine Anmerkung zur Verwendung der Gruppe"
* ^property[=].type = #string
* ^property[+].code = #text
* ^property[=].description = "Ein nicht näher spezifizierter Text, der für die Verwendung des Codes relevant ist"
* insert icd-10-gm_properties_from_meta

RuleSet: icd10gm-babelfsh(input, oid)
* ^identifier.system = "urn:ietf:rfc:3986"
* ^identifier.value = "urn:oid:{oid}"
/*^babelfsh
claml-bfarm
  --path="{input}"
  --meta-to-provide-unmapped=["Para295", "Para301", "MortL1Code", "MortL2Code", "MortL3Code", "MortL4Code", "MorbLCode", "SexCode", "SexReject", "AgeLow", "AgeHigh", "AgeReject", "Exotic", "Content", "Infectious", "EBMLabor"]
^babelfsh*/

CodeSystem: Icd10Gm
Id: icd-10-gm-2024
Description: "asdf"
Title: "ICD-10-GM"
* insert icd10gm-metadata("2024")
* insert icd10gm-babelfsh("./input-files/icd10gm2024syst_claml_20230915.xml", "1.2.276.0.76.5.537")
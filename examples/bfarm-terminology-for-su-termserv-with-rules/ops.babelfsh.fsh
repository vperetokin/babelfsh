RuleSet: ops-metadata(version)
* insert bfarm-sutermserv-tags
* ^meta.profile = "http://hl7.org/fhir/StructureDefinition/shareablecodesystem"
* ^url = "http://fhir.de/CodeSystem/bfarm/ops"
* ^valueSet = "http://fhir.de/ValueSet/bfarm/ops"
* ^version = "{version}"
* ^copyright = "Die Erstellung erfolgt unter Verwendung der maschinenlesbaren Fassung des Bundesinstituts für Arzneimittel und Medizinprodukte (BfArM)."
* ^status = #active
* ^experimental = false
* ^property[+].code = #kind
* ^property[=].description = "Art der Schlüsselnummer, eines von: [chapter, block, category]"
* ^property[=].type = #string
* ^property[+].code = #inclusion
* ^property[=].description = "Ein Inklusisionskriterium"
* ^property[=].type = #string
* ^property[+].code = #exclusion
* ^property[=].description = "Ein Exklusionskriterium"
* ^property[=].type = #string
* ^property[+].code = #definition
* ^property[=].description = "Die Definition des Codes"
* ^property[=].type = #string
* ^property[+].code = #introduction
* ^property[=].description = "Die Einführung eines Kapitels"
* ^property[=].type = #string
* ^property[+].code = #note
* ^property[=].description = "Eine Anmerkung zur Verwendung der Gruppe"
* ^property[=].type = #string
* ^property[+].code = #text
* ^property[=].description = "Ein nicht näher spezifizierter Text, der für die Verwendung des Codes relevant ist"

RuleSet: ops-babelfsh(input, oid)
* ^identifier.system = "urn:ietf:rfc:3986"
* ^identifier.value = "urn:oid:{oid}"
/*^babelfsh
claml-bfarm
  --path="{input}"
  --mode=ops
^babelfsh*/

CodeSystem: OPS
Id: ops-2024
Description: "asdf"
Title: "OPS"
* insert ops-metadata("2024")
* insert ops-babelfsh("./input-files/ops2024syst_claml_20231020.xml", "1.2.276.0.76.5.537")
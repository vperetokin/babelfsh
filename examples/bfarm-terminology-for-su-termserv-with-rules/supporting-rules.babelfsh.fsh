Alias: $sutermserv_project = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project
Alias: $sutermserv_dataset = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset

RuleSet: bfarm-sutermserv-tags
* ^meta.tag[+].system = "$sutermserv_project"
* ^meta.tag[=].code = "bfarm"
* ^meta.tag[=].display = "Bundesinstitut für Arzneimittel und Medizinprodukte"
* ^meta.tag[+].system = "$sutermserv_dataset"
* ^meta.tag[=].code = "bfarm"
* ^meta.tag[=].display = "Bundesinstitut für Arzneimittel und Medizinprodukte"

RuleSet: icd-10-gm_properties_from_meta
* ^property[+].code = #Para295
* ^property[=].description = "Verwendung der Schlüsselnummer nach Paragraph 295 SGB V. P = zur Primärverschlüsselung zugelassene Schlüsselnummer; O = nur als Sternschlüsselnummer zugelassen; Z = nur als Ausrufezeichenschlüsselnummer zugelassen; V = nicht zur Verschlüsselung zugelassen"
* ^property[=].type = #string
* ^property[+].code = #Para301
* ^property[=].description = "Verwendung der Schlüsselnummer nach Paragraph 301 SGB V. P = zur Primärverschlüsselung zugelassene Schlüsselnummer; O = nur als Sternschlüsselnummer zugelassen; Z = nur als Ausrufezeichenschlüsselnummer zugelassen; V = nicht zur Verschlüsselung zugelassen"
* ^property[=].type = #string
* ^property[+].code = #MortL1Code
* ^property[=].description = "Bezug zur Mortalitätsliste 1"
* ^property[=].type = #string
* ^property[+].code = #MortL2Code
* ^property[=].description = "Bezug zur Mortalitätsliste 2"
* ^property[=].type = #string
* ^property[+].code = #MortL3Code
* ^property[=].description = "Bezug zur Mortalitätsliste 3"
* ^property[=].type = #string
* ^property[+].code = #MortL4Code
* ^property[=].description = "Bezug zur Mortalitätsliste 4"
* ^property[=].type = #string
* ^property[+].code = #MorbLCode
* ^property[=].description = "Bezug zur Morbiditätsliste"
* ^property[=].type = #string
* ^property[+].code = #SexCode
* ^property[=].description = "Geschlechtsbezug der Schlüsselnummer; 9 = kein Geschlechtsbezug; M = männlich; W = weiblich"
* ^property[=].type = #string
* ^property[+].code = #SexReject
* ^property[=].description = "Art des Fehlers bei Geschlechtsbezug; 9 = irrelevant; K = Kann-Fehler"
* ^property[+].code = #AgeLow
* ^property[=].description = "untere Altersgrenze für eine Schlüsselnummer. 9999 = irrelevant; t000 - t364 = ab 0 Tage einschließlich Fetalzeit - ab 364 Lebenstagen; j001 - j124 = ab 1 Lebensjahr - ab 124 Lebensjahren"
* ^property[=].type = #string
* ^property[+].code = #AgeHigh
* ^property[=].description = "obere Altersgrenze für eine Schlüsselnummer. 9999 = irrelevant; t000 - t364 = 0 Tage - bis zu 364 Tagen; j001 - j124 = bis zu 1 Jahr – bis zu 124 Jahre"
* ^property[=].type = #string
* ^property[+].code = #AgeReject
* ^property[=].description = "Art des Fehlers bei Altersbezug; 9 = irrelevant; M = Muss-Fehler; K = Kann-Fehler"
* ^property[=].type = #string
* ^property[+].code = #Exotic
* ^property[=].description = "Krankheit in Mitteleuropa sehr selten?"
* ^property[=].type = #boolean
* ^property[+].code = #Content
* ^property[=].description = "Schlüsselnummer mit Inhalt belegt?"
* ^property[=].type = #boolean
* ^property[+].code = #Infectious
* ^property[=].description = "IfSG-Meldung, kennzeichnet, dass bei Diagnosen, die mit dieser Schlüsselnummer kodiert sind, besonders auf die Arzt-Meldepflicht nach dem Infektionsschutzgesetz (IfSG) hinzuweisen ist."
* ^property[=].type = #boolean
* ^property[+].code = #EBMLabor
* ^property[=].description = "IfSG-Labor, kennzeichnet, dass bei Laboruntersuchungen zu diesen Diagnosen die Laborausschlussziffer des EBM (32006) gewählt werden kann."
* ^property[=].type = #boolean
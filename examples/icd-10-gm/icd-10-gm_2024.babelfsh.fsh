CodeSystem: Icd10Gm
Id: icd10gm-2024
Title: "ICD-10-GM"
Description: "Internationale statistische Klassifikation der Krankheiten und verwandter Gesundheitsprobleme, 10. Revision, German Modification"
* ^url = "http://fhir.de/CodeSystem/bfarm/icd-10-gm"
* ^meta.profile = "http://hl7.org/fhir/StructureDefinition/shareablecodesystem"
* ^copyright = "Die Erstellung erfolgt unter Verwendung der maschinenlesbaren Fassung des Bundesinstituts für Arzneimittel und Medizinprodukte (BfArM)."
* ^version = "2024"
* ^status = #active
* ^experimental = false
* ^publisher = "Bundesinstitut für Arzneimittel und Medizinprodukte (BfArM)"
* ^property[+].code = #kind
* ^property[+].description = "Art des Codes"
* ^property[+].type = #string
/*^babelfsh 
claml-bfarm --path='./input-files/icd10gm2024syst_claml_20230915.xml'
^babelfsh*/
CodeSystem: Icd10CmAthena
Id: icd10cm-athena
Title: "ICD-10-WHO"
Description: "A version of the ICD-10-CM generated from the OHDSI Athena repository of standard terminologies."
* ^url = "http://who.int/fhir/CodeSystem/icd-10-cm"
* ^meta.profile = "http://hl7.org/fhir/StructureDefinition/shareablecodesystem"
* ^version = "2024"
* ^status = #active
* ^experimental = false
/*^babelfsh
csv --path='./input_files/icd-10-cm/CONCEPT.csv'
--delimiter='\t'
--code-column='concept_code'
--display-column='concept_name'
--csv-quote='�'
--csv-escape='�'
--concept-filter=[{"column":"vocabulary_id","operator":"=","comparison":"ICD10CM"},{"column":"invalid_reason","operator":"blank"}]
^babelfsh*/
# Example: icd10-who

Simple conversion (without hierarchy) of ICD-10-WHO and ICD-10-CM using a CSV export from OHDSI Athena.
This is a generalizable example: by using different concept filters, other code systems could be generated,
depending on the respective export from Athena.

Do keep in mind that currently, the entire CSV file might be read into memory,
and this process is untested for really large files. It may very well happen that large exports
from Athena will break this process.

This process currently only uses the CONCEPT.csv file for conversion. For including the hierarchy,
a custom plugin for Athena-to-FHIR conversion would need to be written to take
multiple CSV files into account. This isn't terribly difficult, since the default CSV plugin's CsvReader would do
a lot of the work required already. Such a plugin would also be able to take "CONCEPT_SYNONYM"
into account to generate designations for concepts that have multiple names.

## Downloading files

Go to https://athena.ohdsi.org/vocabulary/list and register an account.

Deselect all terminologies, and select those that you need (i.e. for this example, ICD10).
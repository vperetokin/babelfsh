package de.mii_termserv.babelfsh.claml_bfarm

import ca.uhn.fhir.context.FhirContext
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import io.kotest.assertions.assertSoftly
import io.kotest.assertions.withClue
import io.kotest.core.spec.style.FunSpec
import io.kotest.core.spec.style.funSpec
import io.kotest.matchers.Matcher
import io.kotest.matchers.MatcherResult
import io.kotest.matchers.collections.shouldContainAll
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.hl7.fhir.r4b.model.CodeSystem
import java.nio.file.Path
import kotlin.enums.EnumEntries
import kotlin.io.path.Path
import kotlin.io.path.reader
import kotlin.io.path.writeLines


class ClamlBfarmTests : FunSpec({

    fun <T : Enum<T>> testSuite(testName: String, testSuiteInputs: TestSuiteInputs<T>) = funSpec {
        lateinit var referenceCodeMap: Map<String, String>
        lateinit var generatedCodeMap: Map<String, CodeSystem.ConceptDefinitionComponent>

        beforeAny {
            println("Running ClaML test suite for $testName")
            val reader = csvReader {
                delimiter = ';'
                charset = "UTF-8"
                escapeChar = '�'
                quoteChar = '�'
            }
            referenceCodeMap = reader.open(testSuiteInputs.referenceCsv.toFile()) {
                readAllAsSequence(testSuiteInputs.csvHeaders.size).map { row ->
                    testSuiteInputs.csvHeaders.zip(row).toMap()
                }.filter {
                    testSuiteInputs.filterReferenceConcepts(it)
                }.associate {
                    testSuiteInputs.associateReferenceConcepts(it)
                }
            }

            val parser = FHIR_CONTEXT.newJsonParser()
            val codeSystem = parser.parseResource(CodeSystem::class.java, testSuiteInputs.fhirFile.reader())
            generatedCodeMap = codeSystem.concept.associateBy {
                it.code
            }
        }

        fun allCodesPresent() = Matcher<Set<String>> { value ->
            MatcherResult(
                value.isEmpty(),
                { "The following codes are missing: [${value.sorted().joinToString(";")}]" },
                { "All codes are present" })
        }

        test("Generated code map should contain all reference codes") {
            val missingCodes = referenceCodeMap.keys - generatedCodeMap.keys
            missingCodes shouldBe allCodesPresent()
        }

        test("All codes that are generated should be present in the reference file") {
            val concepts = generatedCodeMap.filter { (_, concept) ->
                val kindProperty = concept.property.first { it.code == "kind" }
                kindProperty?.valueStringType?.value == "category"
            }
            concepts.keys shouldContainAll referenceCodeMap.keys
        }

        test("All concepts have a kind property") {
            assertSoftly {
                generatedCodeMap.forEach { (_, concept) ->
                    val kindProperty = concept.property.firstOrNull { it.code == "kind" }
                    kindProperty shouldNotBe null
                }
            }
        }

        test("All concept edges should resolve").config(enabled = false) {
            val keyset = generatedCodeMap.keys.toHashSet()
            assertSoftly {
                generatedCodeMap.forEach { (_, concept) ->
                    val parentProps = concept.property.filter { it.code == "parent" }.map { it.valueCodeType.value }
                    withClue({"Concept ${concept.code} has missing parent properties: ${parentProps.filter { it !in keyset}}"}) {
                        keyset shouldContainAll parentProps
                    }
                    val childrenProps = concept.property.filter { it.code == "child" }.map { it.valueCodeType.value }
                    withClue({"Concept ${concept.code} has missing child properties: ${childrenProps.filter { it !in keyset}}"}) {
                        keyset shouldContainAll childrenProps
                    }
                }
            }
        }

        test("Display string matches the metadata") {
            val mismatches = generatedCodeMap.mapNotNull { (code, concept) ->
                concept.display shouldNotBe null
                return@mapNotNull referenceCodeMap[code]?.let { metadataTitle ->
                    when {
                        concept.display != metadataTitle -> {
                            DisplayMatchOrMismatch(code, metadataTitle, concept.display)
                        }

                        else -> null
                    }
                }
            }

            val mismatchPath = Path("src/test/resources/claml_bfarm/display_mismatches_${testName}.ndjson")
            mismatchPath.writeLines(mismatches.sortedBy { it.code }.map {
                Json.encodeToString(it)
            })
            println("Display mismatches written to $mismatchPath")

            val matchPath = Path("src/test/resources/claml_bfarm/display_matches_${testName}.ndjson")
            val mismatchKeyset = mismatches.map { it.code }.toSet()
            matchPath.writeLines(
                generatedCodeMap.filter { it.key !in mismatchKeyset }.map {
                    val expected = referenceCodeMap[it.key]
                    val actual = when (expected) {
                        null -> generatedCodeMap[it.key]?.display
                        else -> expected
                    }
                    Json.encodeToString(DisplayMatchOrMismatch(it.key, expected = expected, actual = actual!!))
                }
            )
            println("matches written to $matchPath")

            mismatches.map { it.code } shouldBe emptyList()
        }

    }

    include(testSuite("icd-10-gm-2024", TestSuiteInputs.ICD10GM2024))

    include(testSuite("ops-2024", TestSuiteInputs.Ops2024))


}) {
    companion object {
        val FHIR_CONTEXT: FhirContext = FhirContext.forR4B()
    }
}

@Suppress("EnumEntryName")
sealed class TestSuiteInputs<E : Enum<E>>(
    val referenceCsv: Path,
    val fhirFile: Path,
    val csvHeaders: EnumEntries<E>,
    val filterReferenceConcepts: (Map<E, String>) -> Boolean = { true },
    val associateReferenceConcepts: (Map<E, String>) -> Pair<String, String>
) {
    data object ICD10GM2024 : TestSuiteInputs<Icd10Headers>(
        referenceCsv = Path("src/test/resources/claml_bfarm/icd10gm2024syst_header_kodes.csv"),
        fhirFile = Path("src/test/resources/claml_bfarm/cs-icd-10-gm-2024.json"),
        csvHeaders = Icd10Headers.entries,
        associateReferenceConcepts = {
            it[Icd10Headers.schluessel6]!! to it[Icd10Headers.klassentitel]!!
        }
    )

    data object Ops2024 : TestSuiteInputs<OpsHeaders>(
        referenceCsv = Path("src/test/resources/claml_bfarm/ops2024syst_kodes.txt"),
        fhirFile = Path("src/test/resources/claml_bfarm/cs-ops-2024.json"),
        csvHeaders = OpsHeaders.entries,
        associateReferenceConcepts = {
            it[OpsHeaders.schluesselnummer]!! to it[OpsHeaders.klassentitel]!!
        }
    )

    enum class Icd10Headers {
        klassifikationsebene,
        ort_baum,
        art_vierfuenfsteller,
        kapitelnummer,
        dreisteller,
        schluessel7,
        schluessel6,
        schlueesel5,
        klassentitel,
        titel3,
        titel4,
        titel5,
        verwendung_para_259,
        verwendung_para_301,
        mortl1,
        mortl2,
        mortl3,
        mortl4,
        morbi,
        geschlecht,
        geschlecht_fehler,
        alter_unten,
        alter_oben,
        alter_fehler,
        selten,
        belegt,
        ifsg,
        ifsg_labor
    }

    enum class OpsHeaders {
        klassifikationsebene,
        ort_baum,
        art_vierfuenfsteller,
        kapitelnummer,
        erster_dreisteller,
        dreisteller,
        schluesselnummer,
        seitenangabe_erforderlich,
        klassentitel,
        titel_viersteller,
        titel_fuenfsteller,
        titel_sechsteller,
        gueltigkeit_para_17,
        zusatzcode,
        einmalcode
    }
}

@Serializable
data class DisplayMatchOrMismatch(val code: String, val expected: String?, val actual: String)
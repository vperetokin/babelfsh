package de.mii_termserv.babelfsh.plugins.core.claml

import au.csiro.fhir.claml.BfarmClamlMode
import au.csiro.fhir.claml.Claml2FhirResult
import au.csiro.fhir.claml.Claml2FhirResult.Concept.ConceptProperty
import au.csiro.fhir.claml.FhirClamlBfarmConceptService
import au.csiro.fhir.claml.FhirClamlConceptService
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.CodeSystemPlugin
import de.mii_termserv.babelfsh.api.PluginArguments
import org.hl7.fhir.r4b.model.Coding
import java.nio.file.Path
import java.util.*
import org.hl7.fhir.r4b.model.CodeSystem as R4Cs
import org.hl7.fhir.r4b.model.CodeSystem.ConceptDefinitionComponent as R4CsConcept
import org.hl7.fhir.r4b.model.CodeType as R4CodeType
import org.hl7.fhir.r4b.model.StringType as R4StringType
import org.hl7.fhir.r5.model.CodeSystem as R5Cs
import org.hl7.fhir.r5.model.CodeSystem.ConceptDefinitionComponent as R5CsConcept
import org.hl7.fhir.r5.model.CodeType as R5CodeType
import org.hl7.fhir.r5.model.StringType as R5StringType


abstract class ClamlCodeSystemPlugin<ArgType : ClamlArguments>(
    pluginId: String,
    babelfshContext: BabelfshContext,
    pluginHelp: String? = null
) :
    CodeSystemPlugin<ClamlArguments>(
        pluginId = pluginId,
        babelfshContext = babelfshContext,
        pluginHelp = pluginHelp,
        attribution = "This plugin is based on 'fhir-claml' by the Australian e-Health Research Center (AEHRC) at CSIRO, Australia. See github.com/aehrc/fhir-claml for more information."
    ) {
    abstract fun createClamlConverter(args: ClamlArguments): FhirClamlConceptService

    private fun generateFromClaml(args: PluginArguments): Claml2FhirResult {
        val clamlConverter = createClamlConverter(args as ClamlArguments)
        return clamlConverter.babelfshClaml2Fhir() ?: failConversion("Failed to generate FHIR CodeSystem from ClaML")
    }

    @Suppress("DuplicatedCode")
    override fun produceR4(args: PluginArguments, resource: R4Cs): List<R4CsConcept> {
        val clamlResult = generateFromClaml(args as ClamlArguments)
        val data = clamlResult.concepts.filter {
            true
        }.map { clamlConcept ->
            R4CsConcept().apply {
                code = clamlConcept.code
                display = clamlConcept.display
                if (clamlConcept.display != clamlConcept.definition)
                    definition = clamlConcept.definition
                clamlConcept.designations.map { des ->
                    this.addDesignation().apply {
                        language = des.language
                        use = Coding().apply {
                            code = des.useAsString
                        }
                        value = des.value
                    }
                }
                clamlConcept.properties.map { clamlProp ->
                    this.addProperty()
                        .apply {
                            this.code = clamlProp.code
                            when (clamlProp.valueType!!) {
                                ConceptProperty.PropertyValueType.CODE -> this.setValue(R4CodeType(clamlProp.valueAsString))
                                ConceptProperty.PropertyValueType.STRING -> this.setValue(R4StringType(clamlProp.valueAsString))
                            }
                        }
                }
            }
        }.sortedBy { it.code }

        return data
    }

    @Suppress("DuplicatedCode")
    override fun produceR5(
        args: PluginArguments,
        resource: R5Cs
    ): List<R5CsConcept> {
        return generateFromClaml(args).concepts.map {
            R5CsConcept().apply {
                code = it.code
                display = it.display
                if (it.display != it.definition)
                    definition = it.definition
                it.designations.map {
                    this.addDesignation().apply {
                        language = it.language
                        //TODO designation use
                        value = it.value
                    }
                }
                it.properties.map { clamlProp ->
                    this.addProperty()
                        .apply {
                            this.code = clamlProp.code
                            when (clamlProp.valueType!!) {
                                ConceptProperty.PropertyValueType.CODE -> this.setValue(R5CodeType(clamlProp.valueAsString))
                                ConceptProperty.PropertyValueType.STRING -> this.setValue(R5StringType(clamlProp.valueAsString))
                            }
                        }
                }
            }
        }
    }


}

class StandardClamlCodeSystemPlugin(babelfshContext: BabelfshContext) : ClamlCodeSystemPlugin<ClamlArguments.StandardClamlArguments>(
    pluginId = "claml-standard",
    babelfshContext = babelfshContext,
    pluginHelp = buildString {
        append("The arguments --display-rubric, --designation-rubric, --exclude-class-kind can include a ")
        append("comma-separated list to provide multiple arguments. Example: --display-rubric 'foo,bar'")
    },
) {

    override fun parseInto(parser: ArgParser, workingDirectory: Path): ClamlArguments {
        return parser.parseInto { ClamlArguments.StandardClamlArguments(it, workingDirectory) }
    }

    override fun createClamlConverter(args: ClamlArguments): FhirClamlConceptService {
        if (args !is ClamlArguments.StandardClamlArguments) failConversion("Invalid arguments")
        return FhirClamlConceptService(
            args.path.toFile(),
            args.displayRubrics,
            args.definitionRubric,
            args.designationRubrics,
            args.excludeClassKind,
            args.excludeRubricProperties,
            args.metaToPropertyMap,
            args.excludeKindlessClasses,
            args.applyModifiers
        )
    }
}

class ClamlBfarmCodeSystemPlugin(babelfshContext: BabelfshContext) : ClamlCodeSystemPlugin<ClamlArguments.BfarmClamlArguments>(
    pluginId = "claml-bfarm",
    babelfshContext = babelfshContext,
    pluginHelp = buildString {
        append("This plugin is fine-tuned to provide adequate concepts for CodeSystems provided by the German BfArM ")
        append("(Bundesinstitut für Arzneimittel und Medizinprodukte). It may produce results that are not suitable ")
        append("for other ClaML files produced by other agencies. Use the 'claml-standard' plugin for general use cases.")
    }
) {
    override fun parseInto(parser: ArgParser, workingDirectory: Path): ClamlArguments =
        parser.parseInto { ClamlArguments.BfarmClamlArguments(it, workingDirectory) }

    override fun createClamlConverter(args: ClamlArguments): FhirClamlConceptService {
        args as ClamlArguments.BfarmClamlArguments
        return FhirClamlBfarmConceptService(
            /* clamlFile = */ args.path.toFile(),
            /* metaToPropertyMap = */ args.metaToPropertyMap,
            /* mode = */ args.mode
        )
    }

}

sealed class ClamlArguments(parser: ArgParser, workingDirectory: Path) : PluginArguments(parser, workingDirectory) {

    val path: Path by parser.storing(
        "--path",
        "-p",
        help = "Path to the CLAML file"
    ) {
        val path = Path.of(this.preprocess())
        validatePathDelegate(path)
    }

    private val metaToProvideUnmapped by parser.storing(
        "--meta-to-property-passthrough",
    ) {
        jsonArrayToStringList(this)
    }.default(emptyList())

    private val metaToProvideRenamed by parser.storing(
        "--meta-to-property-renamed",
    ) {
        jsonStringToStringMap(this)
    }.default(emptyMap())

    val metaToPropertyMap get() = metaToProvideRenamed + metaToProvideUnmapped.associateWith { it }

    class StandardClamlArguments(parser: ArgParser, workingDirectory: Path) : ClamlArguments(parser, workingDirectory) {
        val displayRubrics: List<String> by parser.storing(
            "--display-rubric",
            "--dis-rub",
            help = buildString {
                append("A comma-separated list of ClaML rubric/s that might contain the ")
                append("concepts' displays. The first populated rubric in the list that is present on a class will be ")
                append("used as the concept's display text. Subsequent rubrics in the list will be used as designations. ")
                append("Default is 'preferred'")
            }
        ) {
            makeList()
        }.default {
            listOf("preferred")
        }

        val definitionRubric: String by parser.storing(
            "--definition-rubric",
            "--def-rub",
            help = buildString {
                append("The ClaML rubric that contains the concepts' definitions. ")
                append("Default is 'definition'")
            }
        ) {
            preprocess()
        }.default("definition")

        val designationRubrics: List<String> by parser.storing(
            "--designation-rubric",
            "--des-rub",
            help = buildString {
                append("A list of ClaML rubrics that contain the concepts' synonyms. Default is empty.")
            }
        ) {
            makeList()
        }.default {
            emptyList()
        }

        val excludeClassKind: List<String> by parser.storing("--exclude-class-kind", help = "The class kind to exclude") {
            makeList()
        }.default {
            emptyList()
        }

        val excludeKindlessClasses: Boolean by parser.mapping(
            "--include-kindless-classes" to false,
            "--exclude-kindless-classes" to true,
            help = "Whether to include kindless classes. Default is to include them."
        ).default(false)

        val excludeRubricProperties: List<String> by parser.storing(
            "--exclude-rubric-properties",
            help = "The rubrics to exclude in the creation of concept properties"
        ) {
            makeList()
        }.default {
            emptyList()
        }

        val applyModifiers: Boolean by parser.mapping(
            "--apply-modifiers" to true,
            "--dont-apply-modifiers" to false,
            help = "Whether to apply modifiers (default is not to apply them)"
        ).default(false)

        override fun toString(): String {
            return "ClamlArguments(path=$path, displayRubrics=$displayRubrics, definitionRubric='$definitionRubric', designationRubrics=$designationRubrics, excludeClassKind=$excludeClassKind, excludeKindlessClasses=$excludeKindlessClasses, applyModifiers=$applyModifiers)"
        }
    }

    class BfarmClamlArguments(parser: ArgParser, workingDirectory: Path) : ClamlArguments(parser, workingDirectory) {

        val mode: BfarmClamlMode by parser.storing(
            "--mode",
            help = "The mode of the ClaML-BfArM plugin. Default is 'NORMAL'. Choices: ${BfarmClamlMode.entries.joinToString(", ")}"
        ) {
            BfarmClamlMode.valueOf(this.preprocess().uppercase(Locale.getDefault()))
        }.default(BfarmClamlMode.NORMAL)

        override fun toString(): String {
            return super.toString().trimEnd(')') + ", mode=$mode)"
        }
    }
}
@file:Suppress("unused")

package de.mii_termserv.babelfsh.plugins.core.csv


import com.github.doyaaaaaken.kotlincsv.dsl.context.CsvReaderContext
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import com.ibm.icu.impl.Assert.fail
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import de.mii_termserv.babelfsh.antlr.service.CommandLineParseException
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.CodeSystemPlugin
import de.mii_termserv.babelfsh.api.ConversionFailedException
import de.mii_termserv.babelfsh.api.PluginArguments
import de.mii_termserv.babelfsh.api.propertymapping.PropertyMapper
import de.mii_termserv.babelfsh.api.propertymapping.PropertyMapperFactory
import de.mii_termserv.babelfsh.api.propertymapping.PropertyMappingException
import kotlinx.serialization.Serializable
import org.hl7.fhir.r4b.model.CodeSystem
import org.slf4j.LoggerFactory
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.nio.file.Path
import org.hl7.fhir.r4b.model.CodeSystem as R4Cs
import org.hl7.fhir.r4b.model.CodeSystem.ConceptDefinitionComponent as R4CsConcept
import org.hl7.fhir.r5.model.CodeSystem as R5Cs
import org.hl7.fhir.r5.model.CodeSystem.ConceptDefinitionComponent as R5CsConcept


class CsvTerminologyReader(
    private val path: Path
) {

    private val logger = LoggerFactory.getLogger(CsvTerminologyReader::class.java)

    fun <T> use(
        headers: List<String>? = null,
        csvConfig: CsvReaderContext.() -> Unit,
        codeColumn: String,
        mapData: (Map<String, String>) -> T,
        conceptFilter: List<CsvFilter>
    ): List<T> {
        val reader = csvReader(init = csvConfig)
        return reader.open(path.toFile()) {
            val data = when (headers) {
                null -> {
                    logger.info("Reading CSV file from $path, using headers from file")
                    this.readAllWithHeaderAsSequence()
                }

                else -> {
                    logger.info("Reading CSV file from $path, using provided headers")
                    this.readAllAsSequence(headers.size).map { row ->
                        headers.zip(row).toMap()
                    }
                }
            }
            return@open filterData(data, conceptFilter).sortedBy { it[codeColumn] }.map(mapData).toList()
        }
    }

    private fun filterData(data: Sequence<Map<String, String>>, conceptFilter: List<CsvFilter>): Sequence<Map<String, String>> {
        return data.filter { row ->
            conceptFilter.all { filter ->
                val columnValue = row[filter.column]
                when (filter.operatorValue) {
                    CsvFilter.ComparisonOperator.EQUALS -> columnValue == filter.comparison
                    CsvFilter.ComparisonOperator.NOT_EQUALS -> columnValue != filter.comparison
                    CsvFilter.ComparisonOperator.NOT_BLANK -> !columnValue.isNullOrBlank()
                    CsvFilter.ComparisonOperator.BLANK -> columnValue.isNullOrBlank()
                }
            }
        }
    }

    companion object {
        fun getCsvConfig(args: CsvArguments): (CsvReaderContext.() -> Unit) {
            return {
                delimiter = args.delimiter
                charset = args.charset.name()
                quoteChar = args.quote
                escapeChar = args.escape
                skipEmptyLine = args.skipEmptyLines
            }
        }
    }

}

class CsvCodeSystemPlugin(babelfshContext: BabelfshContext) :
    CodeSystemPlugin<CsvCodeSystemPlugin.CsvCsArguments>("csv", babelfshContext) {

    private fun <T> readCsv(path: Path, args: CsvCsArguments, mapData: (Map<String, String>) -> T): List<T> {
        val headers = when (args.headers.isEmpty()) {
            true -> null
            false -> args.headers
        }
        return CsvTerminologyReader(path).use(
            headers = headers,
            csvConfig = CsvTerminologyReader.getCsvConfig(args),
            codeColumn = args.codeColumnName,
            mapData = mapData,
            conceptFilter = args.conceptFilter
        )
    }

    override fun parseInto(parser: ArgParser, workingDirectory: Path): CsvCsArguments =
        parser.parseInto { CsvCsArguments(it, workingDirectory) }

    override fun produceR4(args: PluginArguments, resource: R4Cs): List<R4CsConcept> {
        val csvArgs = args as CsvCsArguments
        val data = readCsv(csvArgs.path, csvArgs) { concept ->
            R4CsConcept().apply {
                code = concept[csvArgs.codeColumnName]
                display = concept[csvArgs.displayColumnName]
                addPropertiesR4(csvArgs.propertyMapping, this, concept, resource)
            }
        }
        return data
    }

    private fun addPropertiesR4(
        propertyMapping: List<PropertyMappingEntry>,
        conceptDefinitionComponent: CodeSystem.ConceptDefinitionComponent,
        row: Map<String, String>,
        resource: R4Cs
    ) {
        propertyMapping.forEach { propertyMappingEntry ->
            val columnValue = row[propertyMappingEntry.column]
            when {
                columnValue == null && propertyMappingEntry.required -> {
                    failConversion("Required column ${propertyMappingEntry.column} is missing for concept with code ${conceptDefinitionComponent.code}")
                }

                !columnValue.isNullOrBlank() -> {
                    val property = R4Cs.ConceptPropertyComponent()
                    val matchingProperty = resource.property.find { it.code == propertyMappingEntry.property }
                        ?: throw ConversionFailedException("Property ${propertyMappingEntry.property} not found in CodeSystem")
                    property.code = propertyMappingEntry.property
                    val propertyValueType = matchingProperty.type!!
                    when (propertyMappingEntry.mapper) {
                        null -> propertyMappingEntry.propertyType(propertyValueType).setValueR4(columnValue, property)
                        else -> propertyMappingEntry.mapper.mapper.mapPropertyValueR4(columnValue, property)
                    }
                    conceptDefinitionComponent.addProperty(property)
                }
            }
        }
    }

    override fun produceR5(args: PluginArguments, resource: R5Cs): List<R5CsConcept> {
        TODO("Not yet implemented")
    }

    class CsvCsArguments(parser: ArgParser, workingDirectory: Path) : CsvArguments(parser, workingDirectory) {
        val propertyMapping: List<PropertyMappingEntry> by parser.storing(
            "--property-mapping",
            "--pm",
            help = "Mapping of additional properties to columns in the CSV file. " + PropertyMappingEntry.help(),
        ) {
            propertyMappingFromString(this)
        }.default(listOf())

        private fun propertyMappingFromString(s: String) = decodeJsonArray<PropertyMappingEntry>(
            s,
            "property-mapping",
            "CSV"
        ) { entry ->
            if (entry.column.isBlank() || entry.property.isBlank()) {
                failValidation("Property mappings need to have a non-empty column and property code. Found: $entry")
            }
            entry.mapper?.let {
                try {
                    PropertyMapperFactory.createPropertyMapper(it.id, it.arguments)
                } catch (e: PropertyMappingException) {
                    failValidation("Property mapper ${e.propertyMapper?.id}  was not configured correctly: ${e.message}")
                }
            }
        }

        override fun validateDependentArguments(): Boolean {
            super.validateDependentArguments()
            propertyMapping.forEach {
                if (it.column !in headers) {
                    failValidation("Column ${it.column} not found in headers")
                }
            }
            return true
        }
    }
}

private fun String?.encodeUtf8(from: Charset = StandardCharsets.UTF_16): String? {
    return this?.toByteArray(from)?.toString(StandardCharsets.UTF_8)
}

open class CsvArguments(parser: ArgParser, workingDirectory: Path) : PluginArguments(parser, workingDirectory) {

    val path: Path by parser.storing(
        "--path",
        "-p",
        help = "Path to the CSV file"
    ) {
        val path = Path.of(this.preprocess())
        validatePathDelegate(path)
    }

    val delimiter by parser.storing(
        "--delimiter",
        "-d",
        help = "Delimiter used in the CSV file. Default is comma (,)"
    ) {
        when (val p = this.preprocess()) {
            "\\t" -> '\t'
            else -> p.single()
        }
    }.default(',')

    val quote by parser.storing(
        "--csv-quote",
        help = "Quote character used in the CSV file. Default is double quote (\")"
    ) {
        this.preprocess().single()
    }.default('"')

    val escape by parser.storing(
        "--csv-escape",
        help = "Escape character used in the CSV file. Default is double quote (\")"
    ) {
        this.preprocess().single()
    }.default('"')

    val skipEmptyLines by parser.flagging(
        "--skip-empty-lines",
        "-s",
        help = "Skip empty lines in the CSV file"
    )

    val charset: Charset by parser.storing(
        "--charset",
        "-c",
        help = "The charset of the file, with the name from java.nio.charset.Charset: ${
            Charset.availableCharsets().keys.joinToString(
                ", ",
                limit = 5,
                truncated = "..."
            )
        } See https://docs.oracle.com/javase/8/docs/technotes/guides/intl/encoding.doc.html"
    ) {
        if (!Charset.isSupported(this.preprocess())) fail("Charset $this is not supported")
        Charset.forName(this.preprocess())
    }.default { StandardCharsets.UTF_8 }

    val codeColumnName: String by parser.storing(
        "--code-column",
        "--cc",
        help = "Name of the column containing the codes",
    ) { this.preprocess() }

    val displayColumnName: String by parser.storing(
        "--display-column",
        "--dc",
        help = "Name of the column containing the display names",
    ) { this.preprocess() }

    val lineEnding: String by parser.storing(
        "--line-ending",
        "--le",
        help = "Line ending used in the CSV file, default \\n"
    ) { this.preprocess() }.default("\n")

    val headers: List<String> by parser.storing(
        "--headers",
        "--hd",
        help = "List of headers to use, provided as a JSON list. " +
                "If not provided, headers will be read from the file. " +
                "Example: [\"header1\", \"header2\"]"
    ) {
        try {
            return@storing jsonArrayToStringList(this.trim('\''))
        } catch (e: Exception) {
            throw CommandLineParseException("Could not parse 'headers' parameter for the CSV plugin due to: ${e.message}", null)
        }
    }.default(emptyList())

    val conceptFilter: List<CsvFilter> by parser.storing(
        "--concept-filter",
        "--cf",
        help = "Filter concepts based on the values in the CSV file. ${CsvFilter.operatorHelp()}"
    ) {
        return@storing decodeJsonArray<CsvFilter>(this, "concept-filter", "CSV") {
            if (it.column.isBlank() || it.operator.isBlank()) {
                failValidation("Concept filter needs to have a non-empty column and operator. Found: $it")
            }
            try {
                val operatorValue = it.operatorValue
                if (operatorValue.needsValue && it.comparison.isNullOrBlank()) {
                    failValidation("Operator '${it.operatorValue.name}' requires a comparison value")
                }
            } catch (e: Exception) {
                failValidation(e.message ?: "Unknown error")
            }
        }
    }.default(emptyList())

    override fun validateDependentArguments(): Boolean {
        super.validateDependentArguments()
        if (headers.isNotEmpty() && codeColumnName !in headers) {
            failValidation("Code column $codeColumnName not found in headers")
        }
        if (headers.isNotEmpty() && displayColumnName !in headers) {
            failValidation("Display column $displayColumnName not found in headers")
        }
        return true
    }
}

@Serializable
data class CsvFilter(
    val column: String,
    val operator: String,
    val comparison: String? = null
) {

    enum class ComparisonOperator(val tokens: List<String>, val needsValue: Boolean = true) {
        EQUALS(listOf("=", "==", "equals")),
        NOT_EQUALS(listOf("!=", "<>", "not-equals", "not_equals")),
        NOT_BLANK(listOf("not-blank", "not_blank"), false),
        BLANK(listOf("blank"), false)
    }

    val operatorValue: ComparisonOperator
        get() = ComparisonOperator.entries.find { operator.lowercase() in it.tokens }
            ?: throw CommandLineParseException("Unknown operator $operator", null)

    companion object {
        fun operatorHelp(): String {
            val operators = ComparisonOperator.entries.associate { entry -> entry.name to entry.tokens.joinToString { "'$it'" } }
            return "Supported operators with supported case-insensitive keywords in parentheses: ${operators.entries.joinToString { "${it.key} (${it.value})" }}"
        }
    }
}

@Serializable
data class PropertyMappingEntry(
    val column: String,
    val property: String,
    val required: Boolean = false,
    val mapper: PropertyMapper? = null
) {

    // TODO: maybe the valueType can be provided in the BabelFSH context instead, so it can be read
    // from the FSH rather than the CSV mapping.
    companion object {
        fun help() =
            buildString {
                append("The property mapping is a JSON array of objects, where each object has the following properties: ")
                append("<column>: the name of the column in the CSV file; ")
                append("<code>: the code of the property in the FHIR CodeSystem; ")
                append("<required>: whether the property is required (true or false, default false); ")
                append(
                    "Example: {\"column\": \"my_column\", " +
                            "\"required\": true, " +
                            "\"property\": \"my_property\"}"
                )
            }
    }

    fun propertyType(resourceType: CodeSystem.PropertyType) = CodeSystemPropertyType.valueOf(resourceType.name.uppercase())

    enum class CodeSystemPropertyType(
        val setValueR4: (String, R4Cs.ConceptPropertyComponent) -> Unit,
        val setValueR5: (String, R5Cs.ConceptPropertyComponent) -> Unit
    ) {
        CODE(
            { v, p -> p.setValue(org.hl7.fhir.r4b.model.CodeType(v)) },
            { v, p -> p.setValue(org.hl7.fhir.r5.model.CodeType(v)) }
        ),
        CODING(
            { _, _ -> TODO("Generating Coding property from CSV is not yet implemented") },
            { _, _ -> TODO("Generating Coding property from CSV is not yet implemented") }
        ),
        STRING(
            { v, p -> p.setValue(org.hl7.fhir.r4b.model.StringType(v)) },
            { v, p -> p.setValue(org.hl7.fhir.r5.model.StringType(v)) }
        ),
        INTEGER(
            { v, p -> p.setValue(org.hl7.fhir.r4b.model.IntegerType(v)) },
            { v, p -> p.setValue(org.hl7.fhir.r5.model.IntegerType(v)) }
        ),
        BOOLEAN(
            { v, p -> p.setValue(org.hl7.fhir.r4b.model.BooleanType(v)) },
            { v, p -> p.setValue(org.hl7.fhir.r5.model.BooleanType(v)) }
        ),
        DATETIME(
            { v, p -> p.setValue(org.hl7.fhir.r4b.model.DateTimeType(v)) },
            { v, p -> p.setValue(org.hl7.fhir.r5.model.DateTimeType(v)) }
        ),
        DECIMAL(
            { v, p -> p.setValue(org.hl7.fhir.r4b.model.DecimalType(v)) },
            { v, p -> p.setValue(org.hl7.fhir.r5.model.DecimalType(v)) }
        ),
    }
}
package de.mii_termserv.babelfsh.api

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import de.mii_termserv.babelfsh.antlr.service.CommandLineParseException
import de.mii_termserv.babelfsh.antlr.service.PluginCommentData
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import org.apache.logging.log4j.kotlin.Logging
import org.apache.logging.log4j.kotlin.logger
import java.io.File
import java.nio.file.FileSystemException
import java.nio.file.Path
import org.hl7.fhir.r4b.model.CodeSystem as R4Cs
import org.hl7.fhir.r4b.model.CodeSystem.ConceptDefinitionComponent as R4CsConcept
import org.hl7.fhir.r4b.model.ValueSet as R4Vs
import org.hl7.fhir.r4b.model.ValueSet.ValueSetComposeComponent as R4VsCompose
import org.hl7.fhir.r5.model.CodeSystem as R5Cs
import org.hl7.fhir.r5.model.CodeSystem.ConceptDefinitionComponent as R5CsConcept
import org.hl7.fhir.r5.model.ValueSet as R5Vs
import org.hl7.fhir.r5.model.ValueSet.ValueSetComposeComponent as R5VsCompose

abstract class BabelfshTerminologyPlugin<R4ResourceClass, R5ResourceClass, R4Class, R5Class, ArgType : PluginArguments>(
    val pluginId: String,
    val babelfshContext: BabelfshContext,
    val resourceType: ResourceType,
    val attribution: String?,
    val pluginHelp: String?
) : Logging {

    fun parseArgs(pluginCommentData: PluginCommentData, workingDirectory: Path): ArgType {
        return mainBody {
            val parser = ArgParser(pluginCommentData.pluginCommandLine.toTypedArray())
            try {
                parseInto(parser, workingDirectory)
            } catch (e: Exception) {
                logger.error { "Error parsing plugin arguments: ${e.message}" }
                logger.error { "The command line received was:\n    ${pluginCommentData.pluginCommandLine}" }
                val helpParser = ArgParser(args = arrayOf("--help"), helpFormatter = BabelfshHelpFormatter(this))
                parseInto(helpParser, workingDirectory)
                throw e
            }
        }
    }

    abstract fun parseInto(parser: ArgParser, workingDirectory: Path): ArgType

    abstract fun produceR4(args: PluginArguments, resource: R4ResourceClass): List<R4Class>
    abstract fun produceR5(args: PluginArguments, resource: R5ResourceClass): List<R5Class>

    fun failConversion(s: String): Nothing {
        throw ConversionFailedException(s)
    }

}

class ConversionFailedException(s: String) : IllegalStateException(s)

abstract class CodeSystemPlugin<ArgType : PluginArguments>(
    pluginId: String,
    babelfshContext: BabelfshContext,
    attribution: String? = null,
    pluginHelp: String? = null
) : BabelfshTerminologyPlugin<R4Cs, R5Cs, R4CsConcept, R5CsConcept, ArgType>(
    pluginId = pluginId,
    babelfshContext = babelfshContext,
    resourceType = ResourceType.CodeSystem,
    attribution = attribution,
    pluginHelp = pluginHelp
)

@Suppress("unused")
abstract class ValueSetPlugin<ArgType : PluginArguments>(
    pluginId: String,
    babelfshContext: BabelfshContext,
    attribution: String? = null,
    pluginHelp: String? = null
) : BabelfshTerminologyPlugin<R4Vs, R5Vs, R4VsCompose, R5VsCompose, ArgType>(
    pluginId = pluginId,
    babelfshContext = babelfshContext,
    resourceType = ResourceType.ValueSet,
    attribution = attribution,
    pluginHelp = pluginHelp
)

abstract class PluginArguments(val parser: ArgParser, val workingDirectory: Path) {
    fun String.preprocess(): String = preprocessString(this)
    private fun preprocessString(string: String): String {
        return string.replace("'", "").trim('"').trim()
    }

    open fun validateDependentArguments(): Boolean = true

    fun failValidation(s: String) {
        throw CommandLineExpectationFailed(s)
    }

    protected fun jsonArrayToStringList(
        string: String, validation: ((String) -> Unit)? = {
            if (it.isBlank()) {
                failValidation("Data is blank")
            }
        }
    ): List<String> {
        val data = Json.decodeFromString<List<String>>(string).map {
            it.preprocess().trim()
        }
        data.forEach {
            validation?.invoke(it)
        }
        return data
    }

    fun String.makeList() = split(",").map { it.trim() }.filter { it.isNotBlank() }.toList()

    fun jsonStringToStringMap(s: String, validation: ((Map<String, String>) -> Unit)? = null): Map<String, String> = Json.decodeFromString<Map<String, String>>(s).also {
        validation?.invoke(it)
    }

    fun validatePathDelegate(path: Path): Path {
        val file = path.toFile()
        try {
            validateFileExistsAndIsReadable(file)
            return file.toPath().toAbsolutePath()
        } catch (e: Exception) {
            if (!file.isAbsolute) {
                val resolvedToWorkingDirectory = workingDirectory.resolve(file.toPath())
                try {
                    validateFileExistsAndIsReadable(resolvedToWorkingDirectory.toFile())
                    logger.debug { "Resolved path $path to ${resolvedToWorkingDirectory.toAbsolutePath()}" }
                    return resolvedToWorkingDirectory.toAbsolutePath()
                } catch (e: Exception) {
                    failValidation(e.message ?: "Unknown error")
                }
            }
            failValidation(e.message ?: "Unknown error")
        }
        throw CommandLineExpectationFailed("Could not validate path $path")
    }

    private fun validateFileExistsAndIsReadable(file: File) {
        if (!file.exists()) {
            throw FileSystemException("File $file does not exist")
        }
        if (!file.isFile) {
            throw FileSystemException("File $file is not a file")
        }
        if (!file.canRead()) {
            throw FileSystemException("File $file is not readable")
        }
    }

    inline fun <reified T> decodeJsonArray(s: String, argumentName: String, pluginName: String, validator: (T) -> Unit): List<T> {
        val data = try {
            Json.decodeFromString<List<T>>(s)
        } catch (e: SerializationException) {
            throw CommandLineParseException(
                "Could not parse '$argumentName' parameter for the $pluginName plugin due to: ${e.message}",
                null
            )
        }
        data.forEach { validator.invoke(it) }
        return data
    }

}

class CommandLineExpectationFailed(s: String) : IllegalArgumentException(s)

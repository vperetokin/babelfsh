@file:OptIn(ExperimentalSerializationApi::class)

package de.mii_termserv.babelfsh.api.propertymapping

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import org.hl7.fhir.r4b.model.BooleanType as R4BooleanType
import org.hl7.fhir.r4b.model.CodeSystem as R4Cs
import org.hl7.fhir.r5.model.BooleanType as R5BooleanType
import org.hl7.fhir.r5.model.CodeSystem as R5Cs

@Serializable
data class PropertyMapper(
    val id: String, val arguments: Map<String, String>
) {
    val mapper by lazy { PropertyMapperFactory.createPropertyMapper(id, arguments) }
}

abstract class PropertyMapperImpl(val id: String) {
    abstract fun mapPropertyValueR4(value: String, property: R4Cs.ConceptPropertyComponent)
    abstract fun mapPropertyValueR5(value: String, property: R5Cs.ConceptPropertyComponent)

    abstract fun validateArguments(): String?
}

object PropertyMapperFactory {

    private val propertyMappers: Map<String, (Map<String, String>) -> PropertyMapperImpl> = mapOf("boolean" to { BooleanMapper(it) })
    fun createPropertyMapper(id: String, arguments: Map<String, String>): PropertyMapperImpl =
        propertyMappers[id]?.invoke(arguments)?.let { mapper ->
            mapper.validateArguments()?.let { errorMessage ->
                throw PropertyMappingException(
                    propertyMapper = mapper, message = errorMessage
                )
            }
            mapper
        } ?: throw PropertyMappingException(
            propertyMapper = null, message = "No property mapper found for id $id (supported: ${listPropertyMapperIds()})"
        )

    fun listPropertyMapperIds() = propertyMappers.keys
}

class BooleanMapper(private val arguments: Map<String, String>) : PropertyMapperImpl("boolean") {

    private enum class Keys(val jsonKey: String, val booleanValue: Boolean) {
        TRUE("true", true), FALSE("false", false)
    }

    override fun validateArguments(): String? {
        Keys.entries.forEach { key ->
            if (!arguments.containsKey(key.jsonKey.lowercase())) {
                return "Missing argument for Boolean mapper: ${key.jsonKey}"
            }
            if (arguments.values.groupingBy { it }.eachCount().any { it.value > 1 }) {
                return "Duplicate argument for Boolean mapper: ${key.jsonKey}"
            }
        }
        return null
    }

    private fun toBoolean(value: String): Boolean {
        val mappedValue = arguments.entries.find { it.value == value } ?: throw PropertyMappingException(
            propertyMapper = this, message = "Value $value not found in arguments"
        )
        val booleanValue = Keys.entries.find { it.jsonKey == mappedValue.key.lowercase() }?.booleanValue ?: throw PropertyMappingException(
            propertyMapper = this, message = "Value $value not found in arguments"
        )
        return booleanValue
    }

    override fun mapPropertyValueR4(value: String, property: R4Cs.ConceptPropertyComponent) {
        property.setValue(R4BooleanType(toBoolean(value)))
    }

    override fun mapPropertyValueR5(value: String, property: R5Cs.ConceptPropertyComponent) {
        property.setValue(R5BooleanType(toBoolean(value)))
    }
}

class PropertyMappingException(val propertyMapper: PropertyMapperImpl?, message: String) : Exception(message)
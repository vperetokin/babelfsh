package de.mii_termserv.babelfsh.api

import de.mii_termserv.babelfsh.plugins.core.claml.ClamlBfarmCodeSystemPlugin
import de.mii_termserv.babelfsh.plugins.core.claml.StandardClamlCodeSystemPlugin
import de.mii_termserv.babelfsh.plugins.core.csv.CsvCodeSystemPlugin

abstract class PluginRegistry<PluginClass : BabelfshTerminologyPlugin<*, *, *, *, *>> {
    fun hasPluginId(id: String) = plugins.containsKey(id)

    private val plugins = mutableMapOf<String, () -> PluginClass>()

    fun register(pluginId: String, plugin: () -> PluginClass) {
        plugins[pluginId] = plugin
    }

    fun getPluginConstructor(id: String): (() -> PluginClass)? {
        return plugins[id]
    }

    fun printAllPluginIds(): String {
        return plugins.keys.joinToString(", ") { "'$it'" }
    }
}

object CodeSystemPluginRegistry : PluginRegistry<CodeSystemPlugin<*>>() {
    fun registerAll(babelfshContext: BabelfshContext) {
        register("csv") { CsvCodeSystemPlugin(babelfshContext) }
        register("claml-standard") { StandardClamlCodeSystemPlugin(babelfshContext) }
        register("claml-bfarm") { ClamlBfarmCodeSystemPlugin(babelfshContext) }
    }
}

@Suppress("unused", "UNUSED_PARAMETER")
object ValueSetPluginRegistry : PluginRegistry<ValueSetPlugin<*>>() {
    fun registerAll(babelfshContext: BabelfshContext) {
        // register("csv") { CsvValueSetPlugin(babelfshContext) }
        TODO()
    }
}
package de.mii_termserv.babelfsh.api.resourcefactories

import de.mii_termserv.babelfsh.antlr.service.*
import de.mii_termserv.babelfsh.api.*
import de.mii_termserv.babelfsh.api.resourcefactories.rules.CodeSystemR4RuleEvaluator
import org.apache.logging.log4j.kotlin.Logging
import org.hl7.fhir.r4b.model.CodeSystem
import java.nio.file.Path
import org.hl7.fhir.r4b.model.CodeSystem as R4Cs
import org.hl7.fhir.r5.model.CodeSystem as R5Cs

abstract class ResourceFactory<ResourceClass>(
    val babelfshContext: BabelfshContext, val resourceType: ResourceType
) : Logging {
    private fun createResource(
        resourceData: TsResourceData,
        aliases: List<Alias>,
        plugin: BabelfshTerminologyPlugin<*, *, *, *, *>,
        pluginCommentData: PluginCommentData,
    ): ResourceClass {
        val args = getArgs(plugin, pluginCommentData, resourceData.workingDirectory)

        logger.info { buildString {
            append("Running ${plugin.resourceType.name} plugin '${plugin.pluginId}' for resource with name '${resourceData.name}'. ")
            plugin.attribution?.let {
                append(it)
            }

        }.trim() }
        val resource = initializeResource(resourceData, aliases)
        addContentToResource(resource, args, plugin)
        return resource
    }

    protected abstract fun initializeResource(resourceData: TsResourceData, aliases: List<Alias>): ResourceClass

    protected abstract fun addContentToResource(
        resource: ResourceClass, args: PluginArguments, plugin: BabelfshTerminologyPlugin<*, *, *, *, *>
    )

    private fun getArgs(
        plugin: BabelfshTerminologyPlugin<*, *, *, *, *>,
        pluginCommentData: PluginCommentData,
        workingDirectory: Path,
    ): PluginArguments {
        val args = plugin.parseArgs(pluginCommentData, workingDirectory)
        try {
            args.validateDependentArguments()
        } catch (e: CommandLineExpectationFailed) {
            throw CommandLineParseException("Error parsing plugin arguments: ${e.message}", null)
        }
        return args
    }

    fun createResourceFromFsh(resourceData: TsResourceData, aliases: List<Alias>): ResourceClass {
        logger.info { "Generating $resourceType with name ${resourceData.name} from FSH" }
        val data = resourceData.terminologyPluginComment ?: throw MissingPluginCommentException(resourceType, resourceData.name)
        try {
            val pluginCommentData = PluginCommentPreprocessor(resourceType, data).parseComment()
            val pluginInstance = pluginCommentData.pluginConstructor()
            return createResource(resourceData, aliases, pluginInstance, pluginCommentData)
        } catch (e: CommandLineParseException) {
            logger.error {
                when (e.tokenContext) {
                    null -> e.message
                    else -> "${e.message} (at column: ${e.tokenContext.start?.charPositionInLine}, text: '${e.tokenContext.text}')"
                }
            }
            throw e
        }
    }
}

class MissingPluginCommentException(resourceType: ResourceType, resourceName: String?) :
    IllegalStateException("No plugin comment found for $resourceType with name $resourceName")

abstract class CodeSystemFactory<ResourceClass>(babelfshContext: BabelfshContext) :
    ResourceFactory<ResourceClass>(babelfshContext, ResourceType.CodeSystem) {
    override fun addContentToResource(resource: ResourceClass, args: PluginArguments, plugin: BabelfshTerminologyPlugin<*, *, *, *, *>) {
        val conceptsAdded = when (babelfshContext.releaseVersion) {
            BabelfshContext.ReleaseVersion.R4B -> {
                @Suppress("UNCHECKED_CAST")
                plugin as BabelfshTerminologyPlugin<R4Cs, *, *, *, *>
                plugin.produceR4(args, resource as R4Cs).fold(0L) { acc, value ->
                    (resource as R4Cs).addConcept(value as R4Cs.ConceptDefinitionComponent)
                    acc + 1
                }.also {
                    resource.count = it.toInt()
                }
            }
            else -> {
                @Suppress("UNCHECKED_CAST")
                plugin as BabelfshTerminologyPlugin<*, R5Cs, *, *, *>
                plugin.produceR5(args, resource as R5Cs).fold(0L) { acc, value ->
                    (resource as R5Cs).addConcept(value as R5Cs.ConceptDefinitionComponent)
                    acc + 1
                }.also {
                    resource.count = it.toInt()
                }
            }
        }
        logger.info { "Added $conceptsAdded concepts to CodeSystem ${resourceType.name} (${babelfshContext.releaseVersion.name})" }
    }
}

class CodeSystemR4ResourceFactory(babelfshContext: BabelfshContext) : CodeSystemFactory<R4Cs>(babelfshContext) {

    private val evaluator = CodeSystemR4RuleEvaluator()
    override fun addContentToResource(resource: CodeSystem, args: PluginArguments, plugin: BabelfshTerminologyPlugin<*, *, *, *, *>) {
        @Suppress("UNCHECKED_CAST")
        plugin as BabelfshTerminologyPlugin<R4Cs, *, *, *, *>
        val pluginConcepts = plugin.produceR4(args, resource)
        pluginConcepts.forEach { concept ->
            resource.addConcept(concept as R4Cs.ConceptDefinitionComponent)
        }
        logger.info { "Added ${pluginConcepts.size} concepts to CodeSystem ${resource.name}" }
    }

    override fun initializeResource(resourceData: TsResourceData, aliases: List<Alias>): R4Cs {
        return R4Cs().apply {
            this.name = resourceData.name.stripQuotes()
            this.id = resourceData.id?.stripQuotes()
            this.title = resourceData.title?.stripQuotes()
            evaluator.applyRules(this, resourceData.rules, babelfshContext)
        }
    }
}

//@Suppress("unused")
//class CodeSystemResourceFactory(babelfshContext: BabelfshContext) :
//    ResourceFactory<R4Cs, R5Cs, CodeSystemPlugin<*>>(babelfshContext = babelfshContext, resourceType = ResourceType.CodeSystem), Logging {
//
//    val evaluator = CodeSystemRuleEvaluator()
//
//    override fun initializeR4(resourceData: TsResourceData, aliases: List<Alias>): R4Cs {
//        return R4Cs().apply {
//            this.name = resourceData.name
//            this.id = resourceData.id
//            this.title = resourceData.title
//            evaluator.applyRulesR4(this, resourceData.rules, babelfshContext)
//        }
//    }
//
//    override fun initializeR5(resourceData: TsResourceData, aliases: List<Alias>): R5Cs {
//        return R5Cs().apply {
//            this.name = resourceData.name
//            this.id = resourceData.id
//            this.title = resourceData.title
//        }
//    }
//
//    override fun addContentToResourceR4(
//        resource: R4Cs, args: PluginArguments, plugin: CodeSystemPlugin<*>
//    ): R4Cs = resource.apply {
//        val pluginConcepts = plugin.produceR4(args)
//        pluginConcepts.forEach { concept ->
//            resource.addConcept(concept)
//        }
//    }
//
//    override fun addContentToResourceR5(
//        resource: R5Cs, args: PluginArguments, plugin: CodeSystemPlugin<*>
//    ): R5Cs = resource.apply {
//        val pluginConcepts = plugin.produceR5(args)
//        pluginConcepts.forEach { concept ->
//            resource.addConcept(concept)
//        }
//    }
//}

fun String.stripQuotes() = this.trim('\"', '\'')

fun String.stripCode() = this.stripQuotes().trim('#')

class FshRuleException(message: String, @Suppress("unused") val ruleKey: String): IllegalStateException(message)
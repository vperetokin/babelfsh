package de.mii_termserv.babelfsh.api.resourcefactories.rules


import de.mii_termserv.babelfsh.antlr.service.TsResourceData
import de.mii_termserv.babelfsh.api.BabelfshContext
import org.apache.logging.log4j.kotlin.Logging
import org.hl7.fhir.r4b.model.CodeSystem as R4Cs
import org.hl7.fhir.r4b.model.Enumerations as R4Enums
import org.hl7.fhir.r5.model.CodeSystem as R5Cs
import org.hl7.fhir.r5.model.Enumerations as R5Enums

abstract class RuleEvaluators<ResourceClass> : Logging {

    abstract val simpleRuleMappings: Map<String, RuleEvaluator<ResourceClass>>
    abstract val prefixRuleMappings: Map<String, PrefixRuleEvaluator<ResourceClass, *>>

    fun applyRules(resource: ResourceClass, rules: MutableList<Pair<String, TsResourceData.Rule>>, babelfshContext: BabelfshContext) {
        val prefixRules = rules.filter { (key, _) ->
            prefixRuleMappings.keys.any { prefix -> key.cleanRuleKey().startsWith(prefix) }
        }.map { (key, value) -> key.cleanRuleKey() to value }
        val simpleRules = rules.filter { (key, _) ->
            key.cleanRuleKey() !in prefixRules.map { it.first }
        }
        logger.debug { "Found ${prefixRules.size} prefix rules and ${simpleRules.size} simple rules for release version ${babelfshContext.releaseVersion}" }
        simpleRules.forEach { (ruleKey, ruleValue) ->
            if (ruleValue.value == null) {
                logger.warn { "Rule $ruleKey is null" }
                return@forEach
            }
            val evaluator = simpleRuleMappings[ruleKey.cleanRuleKey()]
                ?: throw IllegalArgumentException("Rule handler for $ruleKey not found for release version ${babelfshContext.releaseVersion}")

            evaluator.setFromRule(resource, ruleValue.value)
            logger.debug {
                val stringResource = babelfshContext.r4Context.newJsonParser().encodeResourceToString(resource as R4Cs)
                "Applied rule $ruleKey to resource: $stringResource"
            }
        }
        prefixRuleMappings.forEach { (prefix, evaluator) ->
            logger.debug { "Applying prefix rules for $prefix" }
            val matchingRules = evaluator.getMatchingRules(prefixRules)
            if (matchingRules.isNotEmpty()) {
                evaluator.applyToResource(resource, matchingRules)
            }
        }
    }
}

fun String.cleanRuleKey() = this.trim().trimStart('^', '*', ' ')

class CodeSystemR4RuleEvaluator : RuleEvaluators<R4Cs>() {
    override val simpleRuleMappings: Map<String, RuleEvaluator<R4Cs>>
        get() = mapOf(
            "url" to StringRuleEvaluator(R4Cs::setUrl),
            "version" to StringRuleEvaluator(R4Cs::setVersion),
            "status" to EnumRuleEvaluator(R4Enums.PublicationStatus.entries) { resource, value ->
                resource.setStatus(value)
            },
            "meta.profile" to StringRuleEvaluator { meta.addProfile(it) },
            "copyright" to StringRuleEvaluator { copyright = it },
            "experimental" to BooleanRuleEvaluator(R4Cs::setExperimental),
            "date" to DateRuleR4Evaluator(R4Cs::setDateElement),
            "publisher" to StringRuleEvaluator(R4Cs::setPublisher),
            "purpose" to StringRuleEvaluator(R4Cs::setPurpose),
            "valueSet" to StringRuleEvaluator(R4Cs::setValueSet),
        )
    override val prefixRuleMappings: Map<String, PrefixRuleEvaluator<R4Cs, *>>
        get() = mapOf(
            "identifier" to IdentifierCodeSystemR4RuleEvaluator(),
            "property" to PropertyCodeSystemR4RuleEvaluator(),
            "meta.tag" to MetaTagR4RuleEvaluator(),
        )
}

@Suppress("unused")
class CodeSystemR5RuleEvaluator : RuleEvaluators<R5Cs>() {
    override val simpleRuleMappings: Map<String, RuleEvaluator<R5Cs>>
        get() = mapOf(
            "meta.profile" to StringRuleEvaluator { meta.addProfile(it) },
            "url" to StringRuleEvaluator(R5Cs::setUrl),
            "version" to StringRuleEvaluator(R5Cs::setVersion),
            "status" to EnumRuleEvaluator(R5Enums.PublicationStatus.entries) { r, v ->
                r.setStatus(v)
            },
            "experimental" to BooleanRuleEvaluator(R5Cs::setExperimental),
            "date" to DateRuleR5Evaluator(R5Cs::setDateElement),
            "copyright" to StringRuleEvaluator { copyright = it },
            "publisher" to StringRuleEvaluator(R5Cs::setPublisher),
            "purpose" to StringRuleEvaluator(R5Cs::setPurpose),
        )
    override val prefixRuleMappings: Map<String, PrefixRuleEvaluator<R5Cs, *>>
        get() = TODO("Not yet implemented")

}
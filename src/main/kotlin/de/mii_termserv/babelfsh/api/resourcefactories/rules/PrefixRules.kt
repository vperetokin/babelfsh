package de.mii_termserv.babelfsh.api.resourcefactories.rules

import de.mii_termserv.babelfsh.antlr.service.TsResourceData
import de.mii_termserv.babelfsh.api.resourcefactories.FshRuleException
import de.mii_termserv.babelfsh.api.resourcefactories.stripCode
import de.mii_termserv.babelfsh.api.resourcefactories.stripQuotes
import org.apache.logging.log4j.kotlin.Logging
import org.hl7.fhir.r4b.model.CodeSystem
import org.hl7.fhir.r4b.model.Identifier
import org.hl7.fhir.r4b.model.CodeSystem as R4Cs
import org.hl7.fhir.r4b.model.Coding as R4Coding
import org.hl7.fhir.r4b.model.DomainResource as R4bDomainResource
import org.hl7.fhir.r4b.model.Identifier as R4Identifier
import org.hl7.fhir.r4b.model.Identifier.IdentifierUse as R4IdentifierUse
import org.hl7.fhir.r4b.model.Meta as R4Meta

abstract class PrefixRuleEvaluator<ResourceType, ParamType>(private val prefix: String) : Logging {

    /**
     * filters rules by prefix
     */
    fun getMatchingRules(rules: List<Pair<String, TsResourceData.Rule>>) =
        rules.filter { (key, _) -> key.startsWith(prefix) }

    /**
     * groups rules by soft indices
     */
    private fun groupRules(rules: List<Pair<String, TsResourceData.Rule>>): List<Map<String, TsResourceData.Rule>> =
        when (rules.any { (key, _) -> key.contains("[") }) {
            false -> {
                val nestedArrayRules = rules.groupingBy { it.first }.eachCount().filter { it.value > 1 }.map { it.key }
                when (nestedArrayRules.isEmpty()) {
                    false -> throw FshRuleException("Multiple rules with same key found", ruleKey = nestedArrayRules.joinToString())
                    else -> listOf(rules.toMap())
                }
            }

            else -> {
                var arrayIndex = -1
                val regexRule = Regex("$prefix\\[(?<index>\\d+|[+=])]\\.(?<suffix>[A-Za-z.]+)")
                val groupedRules = mutableListOf<MutableMap<String, TsResourceData.Rule>>()

                rules.forEach { rule ->
                    val (ruleKey, ruleValue) = rule
                    val match = regexRule.matchEntire(ruleKey)
                    when {
                        match == null -> {
                            val error = when {
                                rule.first.count { it == '[' } > 1 -> "The rule '$ruleKey' features more than one array index"
                                else -> "The rule '$ruleKey' could not be parsed"
                            }
                            throw FshRuleException(error, ruleKey)
                        }

                        else -> {
                            val index =
                                match.groups["index"]?.value ?: throw FshRuleException("No index found in array rule", ruleKey)
                            val suffix =
                                match.groups["suffix"]?.value ?: throw FshRuleException("No suffix found in array rule", ruleKey)
                            when (index) {
                                "+" -> {
                                    logger.debug("+= index: $rule")
                                    arrayIndex += 1
                                }

                                "=" -> {
                                    logger.debug { "= index: $rule" }
                                }

                                else -> {
                                    logger.debug { "index: $index" }
                                    arrayIndex = index.toInt()
                                }
                            }
                            val newKey = "$prefix.$suffix"
                            when (groupedRules.hasIndex(arrayIndex)) {
                                false -> groupedRules.add(mutableMapOf(newKey to ruleValue))
                                else -> groupedRules[arrayIndex][newKey] = ruleValue
                            }
                        }
                    }
                }
                groupedRules
            }
        }

    /**
     * generates an instance of the ParamType from a set of rules
     */
    abstract fun generateParamFromRuleSet(rules: Map<String, TsResourceData.Rule>): ParamType

    /**
     * applies the generated ParamType to the resource
     */
    open fun applyToResource(resource: ResourceType, rules: List<Pair<String, TsResourceData.Rule>>) {
        groupRules(rules).forEach { ruleSet ->
            val property = generateParamFromRuleSet(ruleSet)
            applyToResource(resource, property)
        }
    }

    abstract fun applyToResource(resource: ResourceType, param: ParamType)

    /**
     * used for soft indexing
     */
    private fun List<*>.hasIndex(index: Int) = this.size > index

    fun getRuleValueOrElseThrow(ruleKey: String, rules: Map<String, TsResourceData.Rule>, errorDescription: String = ruleKey): String {
        return rules[ruleKey]?.value?.stripQuotes() ?: throw FshRuleException("$errorDescription not found", ruleKey)
    }
}

abstract class IdentifierR4RuleEvaluator<ResourceType : R4bDomainResource> : PrefixRuleEvaluator<ResourceType, R4Identifier>("identifier") {
    override fun generateParamFromRuleSet(rules: Map<String, TsResourceData.Rule>): R4Identifier {
        val system = rules["identifier.system"]?.value?.stripQuotes()
        val value = rules["identifier.value"]?.value?.stripQuotes()
        val type = rules["identifier.type"]?.value?.stripQuotes()
        val use = rules["identifier.use"]?.value?.stripQuotes()
        val period = rules["identifier.period"]?.value?.stripQuotes()
        return R4Identifier().apply {
            system?.let { this.setSystem(it) }
            value?.let { this.setValue(it) }
            type?.let {
                TODO("type $type to codeable concept")
            }
            use?.let {
                EnumRuleEvaluator(R4IdentifierUse.entries, R4Identifier::setUse).setFromRule(this, it)
                period?.let {
                    TODO("period $period to period")
                }
            }
        }
    }
}

class IdentifierCodeSystemR4RuleEvaluator : IdentifierR4RuleEvaluator<R4Cs>() {
    override fun applyToResource(resource: CodeSystem, param: Identifier) {
        resource.addIdentifier(param)
    }
}

class PropertyCodeSystemR4RuleEvaluator : PrefixRuleEvaluator<R4Cs, R4Cs.PropertyComponent>("property") {

    override fun applyToResource(resource: CodeSystem, param: CodeSystem.PropertyComponent) {
        resource.addProperty(param)
    }

    override fun generateParamFromRuleSet(rules: Map<String, TsResourceData.Rule>): R4Cs.PropertyComponent {
        val code = rules["property.code"]?.value?.stripCode()
        val uri = rules["property.uri"]?.value?.stripQuotes()
        val type = rules["property.type"]?.value?.stripCode()
        val description = rules["property.description"]?.value?.stripQuotes()
        return R4Cs.PropertyComponent().apply {
            code?.let { this.setCode(it) }
            uri?.let { this.setUri(it) }
            type?.let { this.setType(R4Cs.PropertyType.fromCode(it)) }
            description?.let { this.setDescription(it) }
        }
    }
}

class MetaTagR4RuleEvaluator<ResourceType : R4bDomainResource> : PrefixRuleEvaluator<ResourceType, R4Coding>("meta.tag") {
    override fun generateParamFromRuleSet(rules: Map<String, TsResourceData.Rule>): R4Coding {
        val system = getRuleValueOrElseThrow("meta.tag.system", rules)
        val code = getRuleValueOrElseThrow("meta.tag.code", rules)
        val display = rules["meta.tag.display"]?.value?.stripQuotes()
        return R4Coding().apply {
            this.code = code
            this.system = system
            this.display = display
        }
    }

    override fun applyToResource(resource: ResourceType, param: R4Coding) {
        val meta = resource.meta ?: R4Meta().also {
            resource.setMeta(it)
        }
        meta.addTag(param)
    }
}
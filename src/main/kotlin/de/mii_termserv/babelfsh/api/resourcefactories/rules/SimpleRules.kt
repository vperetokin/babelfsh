package de.mii_termserv.babelfsh.api.resourcefactories.rules

import de.mii_termserv.babelfsh.api.resourcefactories.stripQuotes
import kotlin.enums.EnumEntries
import org.hl7.fhir.r4b.model.DateTimeType as R4ModelDateTimeType
import org.hl7.fhir.r5.model.DateTimeType as R5ModelDateTimeType


fun interface RuleEvaluator<RT> {
    fun setFromRule(resource: RT, value: Any)
}

class StringRuleEvaluator<RT>(val setter: RT.(String) -> Unit) : RuleEvaluator<RT> {

    override fun setFromRule(resource: RT, value: Any) {
        setter(resource, (value as String).stripQuotes())
    }
}

class EnumRuleEvaluator<RT, ET : Enum<ET>>(
    private val enumMembers: EnumEntries<ET>,
    val setter: (RT, ET) -> Unit
) :
    RuleEvaluator<RT> {
    override fun setFromRule(resource: RT, value: Any) {
        val nameToFind = (value as String).trim().trimStart('#').uppercase()
        val ourMember =
            enumMembers.find { it.name.uppercase() == nameToFind } ?: throw IllegalArgumentException("Enum member $value not found")
        setter(resource, ourMember)
    }
}

class BooleanRuleEvaluator<RT>(val setter: (RT, Boolean) -> Unit) : RuleEvaluator<RT> {
    override fun setFromRule(resource: RT, value: Any) {
        val boolValue = (value as String).toBoolean()
        setter(resource, boolValue)
    }
}

class DateRuleR4Evaluator<RT>(val setter: (RT, R4ModelDateTimeType) -> Unit) : RuleEvaluator<RT> {
    override fun setFromRule(resource: RT, value: Any) = setter(
        resource,
        R4ModelDateTimeType((value as String).stripQuotes())
    )
}

class DateRuleR5Evaluator<RT>(val setter: (RT, R5ModelDateTimeType) -> Unit) : RuleEvaluator<RT> {
    override fun setFromRule(resource: RT, value: Any) = setter(
        resource,
        R5ModelDateTimeType((value as String).stripQuotes())
    )
}
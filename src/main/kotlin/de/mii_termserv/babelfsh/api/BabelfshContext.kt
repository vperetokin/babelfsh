package de.mii_termserv.babelfsh.api

import ca.uhn.fhir.context.FhirContext

class BabelfshContext(
    val r4Context: FhirContext,
    val r5Context: FhirContext,
    val releaseVersion: ReleaseVersion = ReleaseVersion.R4B,
    val jsonPrettyPrint: Boolean
) {
    enum class ReleaseVersion {
        R4B, R5
    }
}
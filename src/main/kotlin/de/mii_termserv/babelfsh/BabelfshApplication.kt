package de.mii_termserv.babelfsh

import ca.uhn.fhir.context.FhirContext
import com.ibm.icu.impl.Assert.fail
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import com.xenomachina.argparser.mainBody
import de.mii_termserv.babelfsh.antlr.service.FshGrammarService
import de.mii_termserv.babelfsh.antlr.service.FshParseResult
import de.mii_termserv.babelfsh.antlr.service.FshSemanticException
import de.mii_termserv.babelfsh.antlr.service.ParserException
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.CodeSystemPluginRegistry
import de.mii_termserv.babelfsh.api.resourcefactories.CodeSystemR4ResourceFactory
import org.apache.logging.log4j.kotlin.KotlinLogger
import org.apache.logging.log4j.kotlin.Logging
import org.apache.logging.log4j.kotlin.logger
import java.io.File
import org.hl7.fhir.r4b.model.CodeSystem as R4Cs
import org.hl7.fhir.r4b.model.ValueSet as R4Vs
import org.hl7.fhir.r5.model.CodeSystem as R5Cs
import org.hl7.fhir.r5.model.ValueSet as R5Vs

class MainApp {
    companion object : Logging
}

fun main(args: Array<String>) {
    mainBody {
        val mainApp = MainApp()
        parseArguments(args, mainApp.logger).run {
            val babelfshContext = BabelfshContext(
                r4Context = FhirContext.forR4B(),
                r5Context = FhirContext.forR5(),
                releaseVersion = fhirVersion,
                jsonPrettyPrint = jsonPrettyPrint
            )
            CodeSystemPluginRegistry.registerAll(babelfshContext = babelfshContext)
            BabelfshApplication(babelfshContext).runFromFsh(this)
        }
    }
}

private fun parseArguments(args: Array<String>, logger: KotlinLogger): AppArguments {
    try {
        return ArgParser(args).parseInto { parser -> AppArguments(parser, logger) }
    } catch (e: Exception) {
        logger.info { "Error parsing arguments: ${e.message}" }
        ArgParser(arrayOf("--help")).parseInto { parser -> AppArguments(parser, logger) }
        throw e
    }
}

class AppArguments(parser: ArgParser, logger: KotlinLogger) {
    val inputFolder by parser.storing("-i", "--input", help = "Input folder") {
        File(this)
    }.addValidator {
        if (!value.exists()) fail("Input folder does not exist: $value")
        if (!value.isDirectory) fail("Input folder is not a directory: $value")
        if (!value.canRead()) fail("Input folder is not readable: $value")
    }

    val outputFolder by parser.storing("-o", "--output", help = "Output folder") {
        File(this)
    }.addValidator {
        if (!value.exists()) {
            value.mkdirs()
            logger.info { "Created output folder: $value" }
        }
        if (!value.isDirectory) fail("Output folder is not a directory: $value")
        if (!value.canWrite()) fail("Output folder is not writable: $value")
    }

    val excludeFiles by parser.adding("-e", "--exclude", help = "Exclude files with this filename").default(emptyList())

    val fhirVersion by parser.mapping(
        "--r4b" to BabelfshContext.ReleaseVersion.R4B,
        "--r5" to BabelfshContext.ReleaseVersion.R5,
        help = "FHIR version, default: R4B"
    ).default { BabelfshContext.ReleaseVersion.R4B }

    val jsonPrettyPrint by parser.mapping(
        "--pretty-print" to true,
        "--no-pretty-print" to false,
        help = "Pretty print JSON output (default: true)"
    ).default(true).addValidator {
        if (!value) {
            logger.warn { "Pretty-printing is disabled. This will lead to smaller, but very unreadable FHIR resources. Re-enabling pretty-printing is strongly recommended." }
        }
    }
}

class BabelfshApplication(private val babelfshContext: BabelfshContext) : Logging {

    fun runFromFsh(appArguments: AppArguments) {
        try {
            logger.info { "Reading ${appArguments.inputFolder}" }

            val inputFiles = enumerateInputFiles(appArguments.inputFolder).filter {
                it.name !in appArguments.excludeFiles
            }

            val terminologyContent = inputFiles.fold(FshParseResult()) { acc, file ->
                parseFshFile(acc, file.absoluteFile)
            }.resolveAliases().resolveInserts()

            logger.info {
                buildString {
                    append("Processed ${inputFiles.size} files, and found: ")
                    append("${terminologyContent.ruleSets.size} RuleSets, ")
                    append("${terminologyContent.codeSystems.size} CodeSystems, ")
                    append("${terminologyContent.valueSets.size} ValueSets.")
                }
            }

            val convertedData = convertResourceFromFileContent(
                terminologyContent = terminologyContent
            )
            convertedData.codeSystems.filterNotNull().forEach { cs ->
                convertedData.writeContent(appArguments.outputFolder, cs, babelfshContext).also {
                    logger.info { "Wrote CodeSystem to: $it" }
                }
            }
            convertedData.valueSets.filterNotNull().forEach { vs ->
                convertedData.writeContent(appArguments.outputFolder, vs, babelfshContext).also {
                    logger.info { "Wrote ValueSet to: $it" }
                }
            }
        } catch (e: FshSemanticException) {
            handleFshSemanticException(e)
        }
    }


    private fun parseFshFile(acc: FshParseResult, absoluteFile: File): FshParseResult {
        logger.info { "Processing file: $absoluteFile" }
        val fshGrammarService = FshGrammarService(absoluteFile, acc)
        try {
            return fshGrammarService.parseForTerminologyContent()
        } catch (e: ParserException) {
            logger.error { "Unable to parse FSH. ${e.message} (at line: ${e.line}, text: '${e.offendingText}')" }
            throw e
        } catch (e: FshSemanticException) {
            handleFshSemanticException(e)
            throw e
        }
    }

    private fun handleFshSemanticException(e: FshSemanticException) {
        logger.error {
            buildString {
                append("The provided FSH is not semantically correct. ${e.message} ")
                append("(")
                e.file?.let {
                    append("at file: ${it.path}, ")
                }
                e.line?.let {
                    append("at line: ${e.line}, ")
                }
                e.resourceType?.let {
                    append("for $it: ${e.resourceName}")
                }
                append(")")
            }
        }
    }

    private fun enumerateInputFiles(inputFolder: File) = inputFolder.walkTopDown().filter {
        it.extension == "fsh" && "babelfsh" in it.name
    }.toList()

    private fun convertResourceFromFileContent(terminologyContent: FshParseResult): BabelfshResultSet<*, *> {
        return when (babelfshContext.releaseVersion) {
            BabelfshContext.ReleaseVersion.R4B -> {
                val csFactory = CodeSystemR4ResourceFactory(babelfshContext)
                R4ResultSet(
                    codeSystems = terminologyContent.codeSystems.asSequence().map { cs ->
                        logger.info { "Converting CodeSystem '${cs.name}' (id '${cs.id}', defined in ${cs.file.absoluteFile})." }
                        csFactory.createResourceFromFsh(cs, terminologyContent.aliases)
                    },
                    valueSets = terminologyContent.valueSets.asSequence().map {
                        logger.info { "Converting ValueSet '${it.name}' (id '${it.id}', defined in ${it.file.absoluteFile})." }
                        throw NotImplementedError("ValueSets are not yet supported")
                    }
                )
            }

            BabelfshContext.ReleaseVersion.R5 -> {
                TODO()
            }
        }
    }
}

sealed class BabelfshResultSet<CSResourceType, VSResourceType> {
    abstract val codeSystems: Sequence<CSResourceType>
    abstract val valueSets: Sequence<VSResourceType>

    abstract fun getOutputFilenameCs(cs: CSResourceType): String
    abstract fun getOutputFilenameVs(vs: VSResourceType): String

    abstract fun writeContent(outputFolder: File, resource: Any, babelfshContext: BabelfshContext): File
}

class R4ResultSet(
    override val codeSystems: Sequence<R4Cs>,
    override val valueSets: Sequence<R4Vs>
) : BabelfshResultSet<R4Cs, R4Vs>() {
    override fun getOutputFilenameCs(cs: R4Cs): String {
        return "cs-${cs.id}.json"
    }

    override fun getOutputFilenameVs(vs: R4Vs): String {
        return "vs-${vs.id}.json"
    }

    override fun writeContent(outputFolder: File, resource: Any, babelfshContext: BabelfshContext): File {
        val jsonParser = babelfshContext.r4Context.newJsonParser().setPrettyPrint(babelfshContext.jsonPrettyPrint)
        return when (resource) {
            is R4Cs -> outputFolder.resolve(getOutputFilenameCs(resource)).also {
                it.writeText(jsonParser.encodeResourceToString(resource))
            }

            is R4Vs -> outputFolder.resolve(getOutputFilenameVs(resource)).also {
                it.writeText(jsonParser.encodeResourceToString(resource))
            }

            else -> throw IllegalArgumentException("Unknown resource type: ${resource::class.simpleName}")
        }
    }

}

@Suppress("unused")
class R5ResultSet(
    override val codeSystems: Sequence<R5Cs>,
    override val valueSets: Sequence<R5Vs>
) : BabelfshResultSet<R5Cs, R5Vs>() {
    override fun getOutputFilenameCs(cs: R5Cs): String {
        return "cs-${cs.id}.json"
    }

    override fun getOutputFilenameVs(vs: R5Vs): String {
        return "vs-${vs.id}.json"
    }

    override fun writeContent(outputFolder: File, resource: Any, babelfshContext: BabelfshContext): File {
        val jsonParser = babelfshContext.r5Context.newJsonParser().setPrettyPrint(babelfshContext.jsonPrettyPrint)
        return when (resource) {
            is R5Cs -> outputFolder.resolve(getOutputFilenameCs(resource)).also {
                it.writeText(jsonParser.encodeResourceToString(resource))
            }

            is R5Vs -> outputFolder.resolve(getOutputFilenameVs(resource)).also {
                it.writeText(jsonParser.encodeResourceToString(resource))
            }

            else -> throw IllegalArgumentException("Unknown resource type: ${resource::class.simpleName}")
        }
    }
}
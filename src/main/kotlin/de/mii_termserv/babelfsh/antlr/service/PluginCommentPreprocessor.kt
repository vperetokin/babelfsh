package de.mii_termserv.babelfsh.antlr.service

import de.mii_termserv.babelfsh.antlr.BabelFSHCommandLineBaseListener
import de.mii_termserv.babelfsh.antlr.BabelFSHCommandLineLexer
import de.mii_termserv.babelfsh.antlr.BabelFSHCommandLineParser
import de.mii_termserv.babelfsh.api.BabelfshTerminologyPlugin
import de.mii_termserv.babelfsh.api.CodeSystemPluginRegistry
import de.mii_termserv.babelfsh.api.ResourceType
import de.mii_termserv.babelfsh.api.ValueSetPluginRegistry
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.ParseTreeWalker
import org.apache.logging.log4j.kotlin.KotlinLogger
import org.apache.logging.log4j.kotlin.Logging

class PluginCommentPreprocessor(private val resourceType: ResourceType, inputString: String) : Logging {

    private val inputStream = CharStreams.fromString(inputString)
    private val lexer = BabelFSHCommandLineLexer(inputStream)
    private val tokenStream = CommonTokenStream(lexer)
    private val parser = BabelFSHCommandLineParser(tokenStream)
    private val tree = parser.start_rule()
    private val walker = ParseTreeWalker()

    fun parseComment() = WriteablePluginCommentData().apply {
        logger.debug { "Parse tree for command line: ${tree.toStringTree(parser)}" }
        walker.walk(PluginCommentListener(this, resourceType, logger), tree)
    }.toPluginData()

    private class PluginCommentListener(
        private val writeablePluginCommentData: WriteablePluginCommentData,
        private val resourceType: ResourceType,
        val logger: KotlinLogger
    ) : BabelFSHCommandLineBaseListener() {

        override fun enterCommand_line(ctx: BabelFSHCommandLineParser.Command_lineContext) {
            logger.debug { "Parsing command line: ${ctx.text}" }
        }

        override fun enterPlugin_id(ctx: BabelFSHCommandLineParser.Plugin_idContext) {
            val id = ctx.text ?: throw CommandLineParseException("Plugin id is missing", ctx)
            val registry = when (resourceType) {
                ResourceType.CodeSystem -> CodeSystemPluginRegistry
                ResourceType.ValueSet -> ValueSetPluginRegistry
                else -> throw CommandLineParseException("Resource type $resourceType is not supported", ctx)
            }
            val validPluginId = registry.hasPluginId(id)
            if (!validPluginId) {
                throw CommandLineParseException(
                    "Plugin id $id is not registered; known plugins for $resourceType are: ${registry.printAllPluginIds()}",
                    null
                )
            }
            logger.debug { "Plugin id: $id" }
            writeablePluginCommentData.pluginId = id
            writeablePluginCommentData.pluginConstructor = registry.getPluginConstructor(id)
        }

        override fun enterFlagging(ctx: BabelFSHCommandLineParser.FlaggingContext) {
            val name = ctx.arg_name()?.text ?: throw CommandLineParseException("Flag name is missing", ctx)
            writeablePluginCommentData.pluginArgs?.put(name, null)
        }

        override fun enterStoring(ctx: BabelFSHCommandLineParser.StoringContext) {
            val name = ctx.arg_name()?.text ?: throw CommandLineParseException("Storing name is missing", ctx)
            val value = ctx.arg_value()?.text ?: throw CommandLineParseException("Storing value is missing (for $name)", ctx)
            writeablePluginCommentData.pluginArgs?.put(name, value)
        }

    }


    private class WriteablePluginCommentData(
        var pluginId: String? = null,
        val pluginArgs: MutableMap<String, String?>? = mutableMapOf(),
        var pluginConstructor: (() -> BabelfshTerminologyPlugin<*, *, *, *, *>)? = null,
    ) {

        val pluginCommandLine
            get() = pluginArgs?.map { (k, v) ->
                when (v) {
                    null -> k
                    else -> "$k=$v"
                }
            }

        fun toPluginData(): PluginCommentData {
            return PluginCommentData(
                pluginName = pluginId ?: throw CommandLineParseException("Plugin id is missing", null),
                pluginArgs = pluginArgs ?: throw CommandLineParseException("Plugin args are missing", null),
                pluginConstructor = pluginConstructor ?: throw CommandLineParseException("Plugin constructor is missing", null),
                pluginCommandLine = pluginCommandLine ?: throw CommandLineParseException("Plugin command line is missing", null)
            )
        }
    }
}

data class PluginCommentData(
    val pluginName: String,
    val pluginArgs: Map<String, String?>,
    val pluginConstructor: (() -> BabelfshTerminologyPlugin<*, *, *, *, *>),
    val pluginCommandLine: List<String>
)

class CommandLineParseException(message: String, val tokenContext: ParserRuleContext?) : IllegalArgumentException(message)
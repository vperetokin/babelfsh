package de.mii_termserv.babelfsh.antlr.service

import de.mii_termserv.babelfsh.antlr.FSHBaseListener
import de.mii_termserv.babelfsh.antlr.FSHLexer
import de.mii_termserv.babelfsh.antlr.FSHParser
import de.mii_termserv.babelfsh.api.ResourceType
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.ParseTreeWalker
import org.apache.logging.log4j.kotlin.Logging
import org.apache.logging.log4j.kotlin.logger
import java.io.File
import java.nio.file.Path

private const val BABELFSH_COMMENT_PREFIX = "/*^babelfsh"
private const val BABELFSH_COMMENT_SUFFIX = "^babelfsh*/"

class FshGrammarService(private val inputFile: File, private val previousParseResult: FshParseResult) : Logging {

    // TODO support multiple files at the same time by having a temporary file that includes all the files?!
    private val lexer = FSHLexer(CharStreams.fromPath(inputFile.toPath()))
    private val tokens = CommonTokenStream(lexer)
    private val parser = FSHParser(tokens)
    private val parseTree: FSHParser.DocContext = parser.doc()
    private val walker = ParseTreeWalker()
    private val workingDirectory = inputFile.parentFile.toPath().toAbsolutePath()

    fun parseForTerminologyContent(): FshParseResult {
        logger.debug { "Parse tree: ${parseTree.toStringTree(parser)}" }
        val listener = FshListener(inputFile, workingDirectory, previousParseResult)
        walker.walk(listener, parseTree)
        return FshParseResult(
            aliases = listener.aliasList,
            codeSystems = listener.codeSystemList,
            valueSets = listener.valueSetList,
            ruleSets = listener.ruleSets
        )
    }

}

private class FshListener(
    val inputFile: File,
    val workingDirectory: Path,
    previousParseResult: FshParseResult
) : FSHBaseListener(), Logging {

    val aliasList = mutableListOf<Alias>().also {
        it.addAll(previousParseResult.aliases)
    }

    val codeSystemList = mutableListOf<TsResourceData>().also {
        it.addAll(previousParseResult.codeSystems)
    }
    val valueSetList = mutableListOf<TsResourceData>().also {
        it.addAll(previousParseResult.valueSets)
    }

    val ruleSets = mutableListOf<RuleSet>().also {
        it.addAll(previousParseResult.ruleSets)
    }

    var currentResource: TsResourceData? = null
    val currentMode: ResourceType? get() = currentResource?.resourceType

    override fun enterAlias(ctx: FSHParser.AliasContext) {
        logger.trace { "Got alias: ${ctx.text}" }
        val aliasName = ctx.SEQUENCE(0).text.trim()
        if (!aliasName.startsWith("$")) {
            throw ParserStateUnsupportedException(
                buildString {
                    append("Alias name '$aliasName' does not start with '$'. ")
                    append("While this is optional per the FSH specification (section 3.5.2), ")
                    append("BabelFSH only supports alias declarations with a leading '$' to avoid unexpected results.")
                }, ctx
            )
        }
        val aliasValue = when {
            ctx.getTokens(FSHParser.SEQUENCE).size == 2 -> ctx.SEQUENCE(1)
            ctx.CODE() != null -> ctx.CODE()
            else -> throw ParserStateUnsupportedException(
                "Alias value is not set. This may happen if you quote the alias value, which is not allowed in the FSH grammar!",
                ctx
            )
        }.text.trim()
        aliasList.add(Alias(aliasName, aliasValue))
    }

    private fun validateName(name: String) {
        if (NAME_REGEX.matches(name).not()) {
            logger.warn("Name $name does not match the FHIR name regex, this is not recommended.")
        }
        if (name.length > 255) {
            throw FshSemanticException("Name $name is longer than 255 characters", file = inputFile)
        }
    }

    override fun enterCodeSystem(ctx: FSHParser.CodeSystemContext) {
        logger.trace { "Entering CodeSystem" }
        if (currentMode != null) {
            throw ParserStateUnsupportedException("Mode is already set to $currentMode", ctx)
        }
        val name = ctx.name()?.text?.trim() ?: throw ParserStateUnsupportedException("Name is not set", ctx)
        validateName(name)
        currentResource = TsResourceData(
            workingDirectory = workingDirectory,
            file = inputFile,
            resourceType = ResourceType.CodeSystem,
            name = name
        )
    }

    override fun enterValueSet(ctx: FSHParser.ValueSetContext) {
        logger.trace { "Entering ValueSet" }
        if (currentMode != null) {
            throw ParserStateUnsupportedException("Mode is already set to $currentMode", ctx)
        }
        val name = ctx.name()?.text?.trim() ?: throw ParserStateUnsupportedException("Name is not set", ctx)
        validateName(name)
        currentResource = TsResourceData(
            workingDirectory = workingDirectory,
            file = inputFile,
            resourceType = ResourceType.ValueSet,
            name = name
        )
    }

    private fun collisionDeclaredInFile(firstFile: File, secondFile: File): String = when (firstFile.absolutePath) {
        secondFile.absolutePath -> "declared in file $firstFile"
        else -> "declared in files $firstFile and $secondFile"
    }

    override fun enterRuleSet(ctx: FSHParser.RuleSetContext) {
        logger.trace { "Entering RuleSet" }
        if (currentMode != null) {
            throw ParserStateUnsupportedException("Mode is already set to $currentMode", ctx)
        }
        val rulesetName = ctx.RULESET_REFERENCE().text.trim()
        when (val existingRuleSet = ruleSets.find { it.name == rulesetName }) {
            null -> currentResource = RuleSet(workingDirectory = workingDirectory, file = inputFile, name = rulesetName)
            else -> {
                throw ParserStateUnsupportedException(
                    "RuleSet with name '$rulesetName' already exists (${collisionDeclaredInFile(inputFile, existingRuleSet.file)})",
                    ctx
                )
            }
        }
    }

    override fun enterRuleSetRule(ctx: FSHParser.RuleSetRuleContext) {
        logger.debug { "Entering RuleSetRule: '${ctx.text.trim()}'" }
        if (currentMode != ResourceType.RuleSet) {
            throw IllegalStateException("Mode is not set to RuleSet")
        }
        when {
            ctx.sdRule().insertRule() != null -> {
                logger.trace { "Insert in RuleSet ${currentResource!!.name}" }
                val rule: TsResourceData.Rule = when {
                    ctx.sdRule().insertRule().RULESET_REFERENCE() != null -> {
                        val ruleKey = ctx.sdRule().insertRule().RULESET_REFERENCE().text.trim()
                        TsResourceData.Rule(TsResourceData.Rule.RuleType.INSERT, ruleKey)
                    }

                    ctx.sdRule().insertRule().paramRuleSetRef() != null -> {
                        val ruleKey =
                            ctx.sdRule().insertRule().paramRuleSetRef().PARAM_RULESET_REFERENCE().text.trim().removeSuffix("(").trim()
                        val parameters = extractParameters(
                            ctx.sdRule().insertRule().paramRuleSetRef().parameter(),
                            ctx.sdRule().insertRule().paramRuleSetRef().lastParameter()
                        ).map { it.trim('"', '\'') }
                        val rulesetInsertsWithParameters = (currentResource as? RuleSet?)?.parameters?.any { outerParam ->
                            parameters.any { it.contains(outerParam) }
                        } ?: false
                        if (rulesetInsertsWithParameters) (
                                throw FshSemanticException(
                                    "InsertRule parameters must not reference parameters from the ruleset",
                                    file = inputFile,
                                    line = (ctx.start?.line)?.inc()
                                )
                                )
                        TsResourceData.Rule(
                            TsResourceData.Rule.RuleType.INSERT,
                            ruleKey,
                            parameterValues = parameters
                        )
                    }

                    else -> throw ParserStateUnsupportedException("InsertRule is not parseable", ctx)
                }
                currentResource!!.rules.add(rule.value!! to rule)
                currentResource!!.referencedRuleSets.add(rule.value)
            }

            else -> {
                val caretRule = ctx.sdRule().caretValueRule() ?: throw ParserStateUnsupportedException("CaretValueRule is not set", ctx)
                currentResource!!.rules.add(caretRule.caretPath().text to interpretRule(caretRule.value()))
            }
        }
    }

    private fun extractParameters(
        parameters: List<FSHParser.ParameterContext>,
        lastParameter: FSHParser.LastParameterContext
    ): List<String> {
        return parameters.map { it.text.trim().removeSuffix(",").trim() } + lastParameter.text.trim().removeSuffix(")").trim()
    }

    override fun enterParamRuleSet(ctx: FSHParser.ParamRuleSetContext) {
        if (currentMode != null) {
            throw ParserStateUnsupportedException("Mode is already set to $currentMode", ctx)
        }
        val parameters = extractParameters(ctx.paramRuleSetRef().parameter(), ctx.paramRuleSetRef().lastParameter())
        val rulesetName = ctx.paramRuleSetRef().PARAM_RULESET_REFERENCE().text.trim().removeSuffix("(").trim()
        when (val existingRuleSet = ruleSets.find { it.name == rulesetName }) {
            null -> {
                currentResource = RuleSet(
                    workingDirectory = workingDirectory,
                    file = inputFile,
                    name = rulesetName,
                    parameters = parameters.toMutableList()
                )
            }
            else -> {
                throw ParserStateUnsupportedException(
                    "RuleSet with name '$rulesetName' already exists (${collisionDeclaredInFile(inputFile, existingRuleSet.file)})",
                    ctx
                )
            }
        }
    }

    override fun exitCodeSystem(ctx: FSHParser.CodeSystemContext) {
        if (currentMode != ResourceType.CodeSystem) {
            throw IllegalStateException("Mode is not set to CodeSystem")
        }
        if (currentResource?.terminologyPluginComment == null && currentResource?.referencedRuleSets?.none() == true) {
            throw FshSemanticException(
                message = "CodeSystem ${currentResource?.name} does not have a plugin comment or referenced RuleSets",
                resourceType = ResourceType.CodeSystem,
                resourceName = currentResource?.name,
                file = inputFile,
                line = (ctx.start?.line)?.inc()
            )
        }
        codeSystemList.add(currentResource ?: throw ParserStateUnsupportedException("CodeSystem is null", ctx))
        currentResource = null
    }

    override fun exitValueSet(ctx: FSHParser.ValueSetContext) {
        if (currentMode != ResourceType.ValueSet) {
            throw IllegalStateException("Mode is not set to ValueSet")
        }
        if (currentResource?.terminologyPluginComment == null && currentResource?.referencedRuleSets?.none() == true) {
            throw FshSemanticException(
                "ValueSet ${currentResource?.name} does not have a plugin comment or referenced RuleSets",
                ResourceType.ValueSet,
                currentResource?.name,
                inputFile,
                line = (ctx.start?.line)?.inc()
            )
        }
        valueSetList.add(currentResource ?: throw ParserStateUnsupportedException("ValueSet is null", ctx))
        currentResource = null
    }

    private fun onExitRuleset(ctx: ParserRuleContext) {
        if (currentMode != ResourceType.RuleSet) {
            throw IllegalStateException("Mode is not set to RuleSet")
        }
        ruleSets.add((currentResource as RuleSet?) ?: throw ParserStateUnsupportedException("RuleSet is null", ctx))
        currentResource = null
    }

    override fun exitRuleSet(ctx: FSHParser.RuleSetContext) = onExitRuleset(ctx)

    override fun exitParamRuleSet(ctx: FSHParser.ParamRuleSetContext) = onExitRuleset(ctx)

    override fun enterDescription(ctx: FSHParser.DescriptionContext) {
        currentResource?.apply {
            description = when {
                ctx.STRING() != null -> ctx.STRING().text
                ctx.MULTILINE_STRING() != null -> ctx.MULTILINE_STRING().text
                else -> throw ParserStateUnsupportedException("Description is not set", ctx)
            }
        } ?: throw ParserStateUnsupportedException("Resource is not set (at description)", ctx)
    }

    override fun enterTitle(ctx: FSHParser.TitleContext) {
        currentResource?.apply {
            title = ctx.STRING().text
        } ?: throw ParserStateUnsupportedException("Resource is not set (at title)", ctx)
    }

    override fun enterId(ctx: FSHParser.IdContext) {
        if (!ID_REGEX.matches(ctx.name().text)) {
            throw FshSemanticException(
                "Id '${ctx.name().text} 'does not match the FHIR id regex (max 64 characters, [A-Za-z.-] allowed, no exceptions.",
                file = inputFile,
                line = ctx.start.line
            )
        }
        val id = ctx.name().text
        val idCollision = when (currentResource!!.resourceType) {
            ResourceType.CodeSystem -> codeSystemList
            ResourceType.ValueSet -> valueSetList
            else -> throw ParserStateUnsupportedException("Id outside of CodeSystem or ValueSet", ctx)
        }.find { it.id == id }
        if (idCollision != null) {
            throw FshSemanticException(
                "Id $id already exists in ${idCollision.resourceType} ${idCollision.name} (${collisionDeclaredInFile(inputFile, idCollision.file)})",
            )
        }
        currentResource?.apply {
            this.id = id
        } ?: throw ParserStateUnsupportedException("Resource is not set (at id)", ctx)
    }

    override fun enterConcept(ctx: FSHParser.ConceptContext) {
        throw NotSupportedException("Concepts directly", ctx)
    }

    override fun enterCodeInsertRule(ctx: FSHParser.CodeInsertRuleContext) {
        logger.debug { "Entering CodeInsertRule: '${ctx.text.trim()}'" }
        when {
            ctx.paramRuleSetRef() != null -> {
                val ruleSetName = ctx.paramRuleSetRef().PARAM_RULESET_REFERENCE().text.trim().removeSuffix("(").trim()
                val parameterValues = extractParameters(ctx.paramRuleSetRef().parameter(), ctx.paramRuleSetRef().lastParameter()).map {
                    it.trim('"', '\'')
                }
                currentResource?.apply {
                    rules += ruleSetName to TsResourceData.Rule(
                        ruleType = TsResourceData.Rule.RuleType.INSERT,
                        value = ruleSetName,
                        parameterValues = parameterValues
                    )
                    referencedRuleSets.add(ruleSetName)
                }
                logger.trace { "Insert Param with ruleId $ruleSetName and parameters: $parameterValues" }
            }

            else -> {
                val ruleId = ctx.RULESET_REFERENCE().text.trim()
                currentResource?.apply {
                    rules.add(ruleId to TsResourceData.Rule(TsResourceData.Rule.RuleType.INSERT, ruleId))
                    referencedRuleSets.add(ruleId)
                } ?: throw ParserStateUnsupportedException("Resource is not set", ctx)
            }
        }
    }

    override fun enterCodeCaretValueRule(ctx: FSHParser.CodeCaretValueRuleContext) {
        logger.trace { "Entering CodeCaretValueRule: '${ctx.text.trim()}'" }
        if (ctx.CODE() != null && ctx.CODE().size >= 1) {
            throw NotSupportedException(
                "Generating CodeCaretValueRules with CODE is not supported by BabelFSH (example rule: '* #foo ^bar = \"baz\"')", ctx
            )
        }
        val star = ctx.STAR().text
        if (star.contains("  ")) {
            throw NotSupportedException(
                "Generating CodeCaretValueRules with multiple spaces (i.e. reusing parent elements) is not supported by BabelFSH", ctx
            )
        }
        val code = ctx.caretPath().text
        val value = ctx.value()
        val rule = interpretRule(value)
        currentResource?.apply {
            rules.add(code to rule)
            logger.trace { "Added $currentMode rule for $code: $rule" }
        } ?: throw IllegalStateException("Resource is not set")
    }

    private fun interpretRule(value: FSHParser.ValueContext?): TsResourceData.Rule {
        val rule = when {
            value == null -> TsResourceData.Rule(TsResourceData.Rule.RuleType.CODE, null)
            value.code() != null -> TsResourceData.Rule(TsResourceData.Rule.RuleType.CODE, value.code().text)
            value.STRING() != null -> TsResourceData.Rule(TsResourceData.Rule.RuleType.STRING, value.text)
            value.MULTILINE_STRING() != null -> TsResourceData.Rule(TsResourceData.Rule.RuleType.MULTILINE_STRING, value.text)
            value.NUMBER() != null -> TsResourceData.Rule(TsResourceData.Rule.RuleType.NUMBER, value.text)
            value.DATETIME() != null -> TsResourceData.Rule(TsResourceData.Rule.RuleType.DATETIME, value.text)
            value.TIME() != null -> TsResourceData.Rule(TsResourceData.Rule.RuleType.TIME, value.text)
            value.reference() != null -> TsResourceData.Rule(TsResourceData.Rule.RuleType.REFERENCE, value.text)
            value.canonical() != null -> TsResourceData.Rule(TsResourceData.Rule.RuleType.CANONICAL, value.text)
            value.quantity() != null -> TsResourceData.Rule(TsResourceData.Rule.RuleType.QUANTITY, value.text)
            value.ratio() != null -> TsResourceData.Rule(TsResourceData.Rule.RuleType.RATIO, value.text)
            value.bool() != null -> TsResourceData.Rule(TsResourceData.Rule.RuleType.BOOL, value.text)
            value.name() != null -> TsResourceData.Rule(TsResourceData.Rule.RuleType.NAME, value.text)
            else -> throw IllegalStateException("Value is not set")
        }
        return rule
    }

    override fun enterTerminologyPluginComment(ctx: FSHParser.TerminologyPluginCommentContext) {
        logger.trace { "Entering TerminologyPluginComment: ${ctx.text}" }
        val commentText = ctx.TERM_PLUGIN_MULTILINE_COMMENT().text.replace("\n", " ")
            .trim()
            .removePrefix(BABELFSH_COMMENT_PREFIX)
            .removeSuffix(BABELFSH_COMMENT_SUFFIX)
            .trim()
        currentResource?.apply {
            terminologyPluginComment = commentText
        } ?: throw IllegalStateException("Resource is not set")
    }

    override fun enterProfile(ctx: FSHParser.ProfileContext) {
        throw NotSupportedException("Profiles", ctx)
    }

    override fun enterExtension(ctx: FSHParser.ExtensionContext) {
        throw NotSupportedException("Extensions", ctx)
    }

    override fun enterInvariant(ctx: FSHParser.InvariantContext) {
        throw NotSupportedException("Invariants", ctx)
    }

    override fun enterInstance(ctx: FSHParser.InstanceContext) {
        throw NotSupportedException("Instances", ctx)
    }

    override fun enterMapping(ctx: FSHParser.MappingContext) {
        throw NotSupportedException("Mappings", ctx)
    }

    override fun enterLogical(ctx: FSHParser.LogicalContext) {
        throw NotSupportedException("Logical models", ctx)
    }

    override fun enterResource(ctx: FSHParser.ResourceContext) {
        throw NotSupportedException("Resources", ctx)
    }

    override fun exitDoc(ctx: FSHParser.DocContext?) {
        logger.debug { "Finished parsing file" }
    }

    companion object {
        private val ID_REGEX = Regex("^[A-Za-z0-9\\-.]{1,64}$")
        private val NAME_REGEX = Regex("^[A-Z]([A-Za-z0-9_]){1,254}$")
    }
}

class NotSupportedException(
    unsupported: String, context: ParserRuleContext, verb: String = "Generating", suffix: String = "is not supported by BabelFSH"
) : ParserException("${verb.trim()} _${unsupported.trim()}_ ${suffix.trim()}", context)

class ParserStateUnsupportedException(message: String, context: ParserRuleContext) : ParserException(message, context)

sealed class ParserException(message: String, context: ParserRuleContext) : Exception(message) {
    val line = (context.start?.line)?.inc()
    val offendingText = context.text.trim()
}

class FshSemanticException(
    message: String,
    val resourceType: ResourceType? = null,
    val resourceName: String? = null,
    val file: File? = null,
    val line: Int? = null
) : Exception(message)

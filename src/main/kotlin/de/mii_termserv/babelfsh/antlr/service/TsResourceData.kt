package de.mii_termserv.babelfsh.antlr.service

import de.mii_termserv.babelfsh.api.ResourceType
import java.io.File
import java.nio.file.Path


data class Alias(
    val name: String, val value: String
)

open class TsResourceData(
    val workingDirectory: Path,
    val file: File,
    val resourceType: ResourceType,
    val name: String,
    var id: String? = null,
    var title: String? = null,
    var description: String? = null,
    /**
     * not a map, because keys can be duplicated!
     */
    val rules: MutableList<Pair<String, Rule>> = mutableListOf(),
    var terminologyPluginComment: String? = null,
    val referencedRuleSets: MutableList<String> = mutableListOf()
) {

    open fun copyWithRules(newRules: MutableList<Pair<String, Rule>>, newTerminologyComment: String? = terminologyPluginComment): TsResourceData {
        return TsResourceData(
            workingDirectory = this.workingDirectory,
            file = file,
            name = this.name,
            id = this.id,
            title = this.title,
            description = this.description,
            rules = newRules,
            terminologyPluginComment = newTerminologyComment,
            referencedRuleSets = this.referencedRuleSets,
            resourceType = this.resourceType
        )
    }

    data class Rule(
        val ruleType: RuleType, val value: String?, val parameterValues: List<String>? = null
    ) {
        enum class RuleType {
            STRING, MULTILINE_STRING, NUMBER, DATETIME, TIME, REFERENCE, CANONICAL, CODE, QUANTITY, RATIO, BOOL, NAME, INSERT;
        }
    }
}

class RuleSet(
    workingDirectory: Path,
    file: File,
    name: String,
    id: String? = null,
    title: String? = null,
    description: String? = null,
    rules: MutableList<Pair<String, Rule>> = mutableListOf(),
    terminologyPluginComment: String? = null,
    referencedRules: MutableList<String> = mutableListOf(),
    val parameters: MutableList<String> = mutableListOf()
) : TsResourceData(
    workingDirectory = workingDirectory,
    file = file,
    resourceType = ResourceType.RuleSet,
    name = name,
    id = id,
    title = title,
    description = description,
    rules = rules,
    terminologyPluginComment = terminologyPluginComment,
    referencedRuleSets = referencedRules
) {
    override fun copyWithRules(newRules: MutableList<Pair<String, Rule>>, newTerminologyComment: String?): TsResourceData {
        return RuleSet(
            workingDirectory = this.workingDirectory,
            file = this.file,
            name = this.name,
            id = this.id,
            title = this.title,
            description = this.description,
            rules = newRules,
            terminologyPluginComment = this.terminologyPluginComment,
            referencedRules = this.referencedRuleSets,
            parameters = this.parameters
        )
    }
}
/*
CSIRO Open Source Software Licence Agreement (variation of the BSD / MIT License)
Copyright (c) 2020, Commonwealth Scientific and Industrial Research Organisation (CSIRO) ABN 41 687 119 230.
All rights reserved. CSIRO is willing to grant you a licence to this software on the following terms, except where otherwise indicated for third party material.
Redistribution and use of this software in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of CSIRO nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission of CSIRO.
EXCEPT AS EXPRESSLY STATED IN THIS AGREEMENT AND TO THE FULL EXTENT PERMITTED BY APPLICABLE LAW, THE SOFTWARE IS PROVIDED "AS-IS". CSIRO MAKES NO REPRESENTATIONS, WARRANTIES OR CONDITIONS OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY REPRESENTATIONS, WARRANTIES OR CONDITIONS REGARDING THE CONTENTS OR ACCURACY OF THE SOFTWARE, OR OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, THE ABSENCE OF LATENT OR OTHER DEFECTS, OR THE PRESENCE OR ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE.
TO THE FULL EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL CSIRO BE LIABLE ON ANY LEGAL THEORY (INCLUDING, WITHOUT LIMITATION, IN AN ACTION FOR BREACH OF CONTRACT, NEGLIGENCE OR OTHERWISE) FOR ANY CLAIM, LOSS, DAMAGES OR OTHER LIABILITY HOWSOEVER INCURRED.  WITHOUT LIMITING THE SCOPE OF THE PREVIOUS SENTENCE THE EXCLUSION OF LIABILITY SHALL INCLUDE: LOSS OF PRODUCTION OR OPERATION TIME, LOSS, DAMAGE OR CORRUPTION OF DATA OR RECORDS; OR LOSS OF ANTICIPATED SAVINGS, OPPORTUNITY, REVENUE, PROFIT OR GOODWILL, OR OTHER ECONOMIC LOSS; OR ANY SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES, ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT, ACCESS OF THE SOFTWARE OR ANY OTHER DEALINGS WITH THE SOFTWARE, EVEN IF CSIRO HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM, LOSS, DAMAGES OR OTHER LIABILITY.
APPLICABLE LEGISLATION SUCH AS THE AUSTRALIAN CONSUMER LAW MAY APPLY REPRESENTATIONS, WARRANTIES, OR CONDITIONS, OR IMPOSES OBLIGATIONS OR LIABILITY ON CSIRO THAT CANNOT BE EXCLUDED, RESTRICTED OR MODIFIED TO THE FULL EXTENT SET OUT IN THE EXPRESS TERMS OF THIS CLAUSE ABOVE "CONSUMER GUARANTEES".  TO THE EXTENT THAT SUCH CONSUMER GUARANTEES CONTINUE TO APPLY, THEN TO THE FULL EXTENT PERMITTED BY THE APPLICABLE LEGISLATION, THE LIABILITY OF CSIRO UNDER THE RELEVANT CONSUMER GUARANTEE IS LIMITED (WHERE PERMITTED AT CSIRO'S OPTION) TO ONE OF FOLLOWING REMEDIES OR SUBSTANTIALLY EQUIVALENT REMEDIES:
(a)               THE REPLACEMENT OF THE SOFTWARE, THE SUPPLY OF EQUIVALENT SOFTWARE, OR SUPPLYING RELEVANT SERVICES AGAIN;
(b)               THE REPAIR OF THE SOFTWARE;
(c)               THE PAYMENT OF THE COST OF REPLACING THE SOFTWARE, OF ACQUIRING EQUIVALENT SOFTWARE, HAVING THE RELEVANT SERVICES SUPPLIED AGAIN, OR HAVING THE SOFTWARE REPAIRED.
IN THIS CLAUSE, CSIRO INCLUDES ANY THIRD PARTY AUTHOR OR OWNER OF ANY PART OF THE SOFTWARE OR MATERIAL DISTRIBUTED WITH IT.  CSIRO MAY ENFORCE ANY RIGHTS ON BEHALF OF THE RELEVANT THIRD PARTY.
Third Party Components
The following third party components are distributed with the Software.  You agree to comply with the licence terms for these components as part of accessing the Software.  Other third party software may also be identified in separate files distributed with the Software.
___________________________________________________________________
See dependencies in licenses.xml. Details about the licenses of the libraries are generated in target/generated-resources/licenses when building the application.
___________________________________________________________________
This file was adapted from: https://github.com/aehrc/fhir-claml
*/

package au.csiro.fhir.claml;

import au.csiro.fhir.claml.model.claml.*;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Stream;


public class FhirClamlBfarmConceptService extends FhirClamlConceptService {

    final static List<String> DISPLAY_RUBRICS = List.of("preferredLong", "preferred");
    final static String DEFINITION_RUBRIC = "display";
    final static List<String> DESIGNATION_RUBRICS = List.of("preferredLong");
    final static List<String> EXCLUDE_CLASS_KINDS = List.of();
    final static List<String> EXCLUDE_RUBRIC_PROPERTIES = List.of("modifierlink");
    final static boolean EXCLUDE_KINDLESS_CLASSES = false;
    final static boolean APPLY_MODIFIERS = true;

    private final BfarmClamlMode clamlMode;

    public FhirClamlBfarmConceptService(File clamlFile, Map<String, String> metaToPropertyMap, BfarmClamlMode mode) {
        super(clamlFile,
                DISPLAY_RUBRICS,
                DEFINITION_RUBRIC,
                DESIGNATION_RUBRICS,
                EXCLUDE_CLASS_KINDS,
                EXCLUDE_RUBRIC_PROPERTIES,
                metaToPropertyMap,
                EXCLUDE_KINDLESS_CLASSES,
                APPLY_MODIFIERS);
        this.clamlMode = mode;
    }

    @Override
    protected String getLabelValue(Reference reference, boolean forDisplay) {
        if (forDisplay) {
            return "";
        }
        String referenceValue = String.format("(%s)", reference.getContent());
        return String.format(" %s", referenceValue);
    }

    @Override
    protected String getLabelValue(Fragment fragment, boolean forDisplay) {
        List<String> parts = fragment.getContent().stream().map(c -> {
            switch (c) {
                case null -> throw new IllegalArgumentException("Fragment contains null content");
                case Reference reference -> {
                    return getLabelValue(reference, forDisplay);
                }
                case String s -> {
                    return s;
                }
                default -> {
                    return "- %s".formatted(getLabelValue(c, forDisplay));
                }
            }
        }).toList();
        return String.join(" ", parts);
    }

    @Override
    protected String getLabelValue(Label label, boolean forDisplay) {
        Pattern extranousWhitespace = Pattern.compile("\\s{2,}");
        List<String> parts = label.getContent().stream()
                .map(l -> getLabelValue(l, forDisplay)).toList();
        List<String> cleanedParts = parts.stream()
                .map(s -> s.replaceAll(extranousWhitespace.pattern(), " "))
                .toList();
        return String.join("", cleanedParts)
                .replaceAll("^-\\s*", "") // remove leading hyphens for Fragment rubrics
                .replaceAll(extranousWhitespace.pattern(), " ") // do it again in case two spaces were joined together above
                .trim();
    }

    @Override
    protected String getLabelValue(Term term, boolean forDisplay) {
        switch (term.getClazz()) {
            case "tab" -> {
                return "\t";
            }
            case "subscript", "superscript", "italics", "bold" -> {
                return term.getContent();
            }
            default -> {
                log.warn("Unrecognized Term class:" + term.getClazz());
                return term.getContent();
            }
        }
    }

    private String generateDisplay(Claml2FhirResult.Concept concept, Label label, Map<String, Claml2FhirResult.Concept> concepts) {
        switch (clamlMode) {
            case NORMAL -> {
                return getLabelValue(label, true).trim();
            }
            case OPS -> {
                if (concept.getSingleRubric("preferredLong") != null && !concept.getSingleRubric("preferredLong").isEmpty()) {
                    return getLabelValue(concept.getSingleRubric("preferredLong").getFirst(), true).trim();
                }
                ArrayList<Claml2FhirResult.Concept> ancestors = new ArrayList<>();
                ancestors.add(concept); // add the concept itself (since it is not in the list of parent codes)
                Optional<Claml2FhirResult.Concept> ancestor = getClassByAncestorCode(concept, concepts);
                while (ancestor.isPresent()) {
                    ancestors.addFirst(ancestor.get());
                    ancestor = getClassByAncestorCode(ancestor.get(), concepts);
                }
                if (ancestors.isEmpty()) {
                    return getLabelValue(label, true).trim();
                }
                List<Pair<String, Label>> labelStream = ancestors.stream()
                        .map(this::getPreferredDisplayFromConcept)
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .toList();
                List<Pair<String, Label>> preferredLong = labelStream.stream().filter(p -> p.getKey().equals("preferredLong")).toList();
                if (preferredLong.isEmpty()) {
                    return String.join(": ", labelStream.stream().map(p -> getLabelValue(p.getValue(), true)).toList());
                } else {
                    int i = labelStream.indexOf(preferredLong.getLast());
                    List<Pair<String, Label>> pairs = labelStream.subList(i, labelStream.size());
                    return String.join(": ", pairs.stream().map(p -> getLabelValue(p.getValue(), true)).toList());
                }
            }
        }
        throw new IllegalArgumentException("Unknown BfarmClamlMode: " + clamlMode);
    }

    @Override
    protected String generateDisplayForConceptFromLabel(Claml2FhirResult.Concept concept, Label label, Map<String, Claml2FhirResult.Concept> concepts) {
        switch (clamlMode) {
            case NORMAL -> {
                return super.generateDisplayForConceptFromLabel(concept, label, concepts);
            }
            case OPS -> {
                Optional<Claml2FhirResult.Concept> classByAncestorCode = getClassByAncestorCode(concept, concepts);
                return classByAncestorCode.map(value -> generateDisplay(concept, label, concepts)).orElseGet(() -> super.generateDisplayForConceptFromLabel(concept, label, concepts));
            }
        }
        throw new IllegalArgumentException("Unknown BfarmClamlMode: " + clamlMode);
    }

    private Optional<Claml2FhirResult.Concept> getClassByAncestorCode(Claml2FhirResult.Concept c, Map<String, Claml2FhirResult.Concept> concepts) {
        Optional<String> ancestorCode = getAncestorCode(c);
        if (ancestorCode.isEmpty()) {
            return Optional.empty();
        }
        Claml2FhirResult.Concept parentConcept = concepts.get(ancestorCode.get());
        return Optional.ofNullable(parentConcept);
    }

    private Optional<String> getAncestorCode(Claml2FhirResult.Concept c) {
        String ancestorCode = c.getCode();
        if (ancestorCode.contains("...")) { //groups in OPS
            return Optional.empty();
        }
        if (ancestorCode.length() < 5) {
            return Optional.empty();
        }
        while (ancestorCode.length() > 5) {
            ancestorCode = ancestorCode.substring(0, ancestorCode.length() - 1);
            char lastChar = ancestorCode.charAt(ancestorCode.length() - 1);
            if (lastChar == '.') {
                continue;
            }
            return Optional.of(ancestorCode);
        }
        return Optional.empty();
    }

    @Override
    protected String getLabelValueForMultipleRubrics(List<Rubric> rubrics, String dr, ModifierClass modClass, String modifiedConcept) {
        List<String> rubricLabels = rubrics.stream().map(r -> getLabelValue(r.getLabel().getFirst(), true)).toList();
        return String.join(": ", rubricLabels);
    }

    @Override
    protected String generateDisplayForModifiedConcept(
            Claml2FhirResult.Concept candidate,
            Claml2FhirResult.Concept modifiedConcept,
            String displayRubricValue,
            ModifierClass modClass, Map<String, Claml2FhirResult.Concept> concepts) {

        Optional<Rubric> modDisplay = getPreferredDisplayFromRubrics(modClass.getRubric());
        if (modDisplay.isEmpty()) {
            log.warn("No display rubric found for modifier class: " + modClass.getCode());
            return displayRubricValue;
        }



        return switch (clamlMode) {
            case NORMAL -> {
                String modifiedConceptDisplay = candidate.getDisplay();
                // we do not use a super call here since the default implementation joins like this: "a : b" (with a space before the colon)
                // bfarm doesn't do that, so we need to adjust the format
                String modDisplayValue = getLabelValue(modDisplay.get().getLabel().getFirst(), true);
                yield "%s: %s".formatted(modifiedConceptDisplay, modDisplayValue);
            }
            case OPS -> {
                String generated = generateDisplay(candidate, modDisplay.get().getLabel().getFirst(), concepts);
                yield "%s: %s".formatted(generated, displayRubricValue);
            }
        };

    }


    @NotNull
    private Optional<Rubric> getPreferredDisplayFromRubrics(List<Rubric> rubrics) {
        Stream<Rubric> optionalStream = displayRubrics.stream()
                .map(dr -> rubrics.stream().filter(r -> Objects.equals(((RubricKind) r.getKind()).getName(), dr))
                        .findFirst()).filter(Optional::isPresent).map(Optional::get);
        return optionalStream.findFirst();
    }

    @NotNull
    private Optional<Pair<String, Label>> getPreferredDisplayFromConcept(Claml2FhirResult.Concept concept) {
        return concept.getRubrics().entrySet().stream()
                .filter(c -> displayRubrics.contains(c.getKey()))
                .map(e -> Pair.of(e.getKey(), e.getValue().getFirst())).findFirst();
    }

//    private Claml2FhirResult displayPostProcessing(Claml2FhirResult inputResult, List<String> displayRubrics) {
//        if (!applyDisplayPostProcessing) {
//            return inputResult;
//        }
//
//        LinkedList<Claml2FhirResult.Concept> modifiedConcepts = new LinkedList<>();
//        String preferredDisplayRubrics = displayRubrics.getFirst();
//
//        //first, add all concepts to the output list that have a rubric that is the preferred one for use as the display (often: preferredLong)
//        inputResult.concepts().stream()
//                .filter(c -> c.getDisplayRubric().equals(preferredDisplayRubrics))
//                .forEach(modifiedConcepts::add);
//
//        Map<String, String> cleanCodeDisplayMap = inputResult.concepts().stream()
//                .filter(ic -> ic.getPropertyValue("kind").equals("category"))
//                .collect(Collectors.toMap(Claml2FhirResult.Concept::getCodeWithoutSpecialCharacters, Claml2FhirResult.Concept::getDisplay));
//        //then, for all other concepts, add them to the output list, but modify their display to include the ancestor's display
//        Stream<Claml2FhirResult.Concept> conceptStream = inputResult.concepts().stream().filter(c -> !c.getDisplayRubric().equals(preferredDisplayRubrics));
//        conceptStream.forEach(c -> {
//            if (List.of("block", "chapter").contains(c.getPropertyValue("kind"))) {
//                modifiedConcepts.add(c);
//                return;
//            }
//            ArrayList<String> parentCodes = getParentCodes(c);
//            Stream<String> sortedParentCodes = parentCodes.stream().sorted();
//            // since all codes are prefixes of one another, the sort will ensure that the shortest code is first
//            List<String> matchingDisplays = sortedParentCodes.map(cleanCodeDisplayMap::get).toList();
//            if (matchingDisplays.size() == 1) {
//                modifiedConcepts.add(c);
//                return;
//            } else if (matchingDisplays.isEmpty()) {
//                log.warn("No matching display found for concept: " + c.getCode());
//                modifiedConcepts.add(c);
//                return;
//            }
//            Stream<String> displayParts = IntStream.range(0, matchingDisplays.size()).mapToObj(i -> {
//                if (i == 0) {
//                    return matchingDisplays.get(i);
//                }
//                String thisDisplay = matchingDisplays.get(i);
//                List<String> previousDisplays = matchingDisplays.subList(0, i);
//                return previousDisplays.stream().reduce(thisDisplay, (acc, pd) -> {
//                    if (acc.startsWith(pd)) {
//                        return acc.replace(pd, "").replaceAll("^: ", "");
//                    }
//                    return acc;
//                });
//            });
//            String newDisplay = displayParts.collect(Collectors.joining(": "));
//            Claml2FhirResult.Concept newConcept = new Claml2FhirResult.Concept(c.getCode());
//            newConcept.setDisplay(newDisplay, c.getDisplayRubric());
//            c.getProperties().forEach(p -> newConcept.addProperty(p.code(), p.valueAsString(), p.valueType()));
//            c.getDesignations().forEach(d -> newConcept.addDesignation(d.value(), d.language(), d.useAsString()));
//            newConcept.setDefinition(c.getDefinition());
//            modifiedConcepts.add(newConcept);
//        });
//
//        return new Claml2FhirResult(inputResult.title(), inputResult.properties(), modifiedConcepts);
//    }

//    @NotNull
//    private ArrayList<String> getParentCodes(Claml2FhirResult.Concept c) {
//        ArrayList<String> parentCodes = new ArrayList<>();
//        parentCodes.add(c.getCodeWithoutSpecialCharacters()); // add the code itself (since it is not in the list of parent codes
//        String parentCode = c.getCodeWithoutSpecialCharacters().substring(0, c.getCodeWithoutSpecialCharacters().length() - 1); // -2 because length is off by one and we want the code without the last character
//        while (parentCode.length() >= lengthOfShortestCode) {
//            parentCodes.add(parentCode);
//            parentCode = parentCode.substring(0, parentCode.length() - 1);
//        }
//        return parentCodes;
//    }
}


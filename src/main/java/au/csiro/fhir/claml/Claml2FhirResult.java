package au.csiro.fhir.claml;

import au.csiro.fhir.claml.model.claml.Label;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public record Claml2FhirResult(
        String title,
        List<CsProperty> properties,
        List<Concept> concepts
) {

    public record CsProperty(
            String rubricCode,
            String rubricDisplay
    ) {
    }

    public static class Concept {

        private final String code;
        private String display = null;
        private String definition = null;


        private final ArrayList<ConceptProperty> properties = new ArrayList<>();
        private final ArrayList<ConceptDesignation> designations = new ArrayList<>();

        private final HashMap<String, ArrayList<Label>> rubrics = new HashMap<>();

        public Concept(String code) {
            this.code = code;
        }

        public void addProperty(String code, String valueAsString, ConceptProperty.PropertyValueType valueType) {
            properties.add(new ConceptProperty(code, valueAsString, valueType));
        }

        public void addDesignation(String value, String language, String useAsString) {
            designations.add(new ConceptDesignation(value, language, useAsString));
        }

        public void setDisplay(String display) {
            this.display = display;
        }

        public void addRubric(String code, Label label) {
            ArrayList<Label> rubricValues = rubrics.getOrDefault(code, new ArrayList<>());
            rubricValues.add(label);
            rubrics.put(code, rubricValues);
        }

        public Pair<List<Label>, String> getPreferredRubric(List<String> rubricPreferences) {
            for (String rubricPreference : rubricPreferences) {
                if (rubrics.containsKey(rubricPreference)) {
                    return Pair.of(rubrics.get(rubricPreference), rubricPreference);
                }
            }
            return null;
        }

        public void setDefinition(String definition) {
            this.definition = definition;
        }

        public List<ConceptProperty> getProperties() {
            return properties;
        }

        public List<ConceptDesignation> getDesignations() {
            return designations;
        }

        public String getCode() {
            return code;
        }

        public String getDefinition() {
            return definition;
        }

        public String getDisplay() {
            return display;
        }

        public boolean noDisplay() {
            return display == null || display.isBlank();
        }

        public boolean noDefinition() {
            return definition == null || definition.isBlank();
        }

        public String getPropertyValue(String kind) {
            return properties.stream().filter(p -> p.code.equals(kind)).findFirst().map(ConceptProperty::valueAsString).orElse(null);
        }

        public ArrayList<Label> getSingleRubric(String definitionRubric) {
            if (definitionRubric == null) return null;
            return rubrics.getOrDefault(definitionRubric, null);
        }

        public Map<String, ArrayList<Label>> getRubrics() {
            return rubrics;
        }

        public record ConceptProperty(
                String code,
                String valueAsString,
                PropertyValueType valueType
        ) {
            public enum PropertyValueType {
                STRING,
                CODE
            }
        }

        public record ConceptDesignation(
                String value,
                String language,
                String useAsString
        ) {
        }

    }
}

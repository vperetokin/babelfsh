/*
CSIRO Open Source Software Licence Agreement (variation of the BSD / MIT License)
Copyright (c) 2020, Commonwealth Scientific and Industrial Research Organisation (CSIRO) ABN 41 687 119 230.
All rights reserved. CSIRO is willing to grant you a licence to this software on the following terms, except where otherwise indicated for third party material.
Redistribution and use of this software in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of CSIRO nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission of CSIRO.
EXCEPT AS EXPRESSLY STATED IN THIS AGREEMENT AND TO THE FULL EXTENT PERMITTED BY APPLICABLE LAW, THE SOFTWARE IS PROVIDED "AS-IS". CSIRO MAKES NO REPRESENTATIONS, WARRANTIES OR CONDITIONS OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY REPRESENTATIONS, WARRANTIES OR CONDITIONS REGARDING THE CONTENTS OR ACCURACY OF THE SOFTWARE, OR OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, THE ABSENCE OF LATENT OR OTHER DEFECTS, OR THE PRESENCE OR ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE.
TO THE FULL EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL CSIRO BE LIABLE ON ANY LEGAL THEORY (INCLUDING, WITHOUT LIMITATION, IN AN ACTION FOR BREACH OF CONTRACT, NEGLIGENCE OR OTHERWISE) FOR ANY CLAIM, LOSS, DAMAGES OR OTHER LIABILITY HOWSOEVER INCURRED.  WITHOUT LIMITING THE SCOPE OF THE PREVIOUS SENTENCE THE EXCLUSION OF LIABILITY SHALL INCLUDE: LOSS OF PRODUCTION OR OPERATION TIME, LOSS, DAMAGE OR CORRUPTION OF DATA OR RECORDS; OR LOSS OF ANTICIPATED SAVINGS, OPPORTUNITY, REVENUE, PROFIT OR GOODWILL, OR OTHER ECONOMIC LOSS; OR ANY SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES, ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT, ACCESS OF THE SOFTWARE OR ANY OTHER DEALINGS WITH THE SOFTWARE, EVEN IF CSIRO HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM, LOSS, DAMAGES OR OTHER LIABILITY.
APPLICABLE LEGISLATION SUCH AS THE AUSTRALIAN CONSUMER LAW MAY APPLY REPRESENTATIONS, WARRANTIES, OR CONDITIONS, OR IMPOSES OBLIGATIONS OR LIABILITY ON CSIRO THAT CANNOT BE EXCLUDED, RESTRICTED OR MODIFIED TO THE FULL EXTENT SET OUT IN THE EXPRESS TERMS OF THIS CLAUSE ABOVE "CONSUMER GUARANTEES".  TO THE EXTENT THAT SUCH CONSUMER GUARANTEES CONTINUE TO APPLY, THEN TO THE FULL EXTENT PERMITTED BY THE APPLICABLE LEGISLATION, THE LIABILITY OF CSIRO UNDER THE RELEVANT CONSUMER GUARANTEE IS LIMITED (WHERE PERMITTED AT CSIRO'S OPTION) TO ONE OF FOLLOWING REMEDIES OR SUBSTANTIALLY EQUIVALENT REMEDIES:
(a)               THE REPLACEMENT OF THE SOFTWARE, THE SUPPLY OF EQUIVALENT SOFTWARE, OR SUPPLYING RELEVANT SERVICES AGAIN;
(b)               THE REPAIR OF THE SOFTWARE;
(c)               THE PAYMENT OF THE COST OF REPLACING THE SOFTWARE, OF ACQUIRING EQUIVALENT SOFTWARE, HAVING THE RELEVANT SERVICES SUPPLIED AGAIN, OR HAVING THE SOFTWARE REPAIRED.
IN THIS CLAUSE, CSIRO INCLUDES ANY THIRD PARTY AUTHOR OR OWNER OF ANY PART OF THE SOFTWARE OR MATERIAL DISTRIBUTED WITH IT.  CSIRO MAY ENFORCE ANY RIGHTS ON BEHALF OF THE RELEVANT THIRD PARTY.
Third Party Components
The following third party components are distributed with the Software.  You agree to comply with the licence terms for these components as part of accessing the Software.  Other third party software may also be identified in separate files distributed with the Software.
___________________________________________________________________
See dependencies in licenses.xml. Details about the licenses of the libraries are generated in target/generated-resources/licenses when building the application.
___________________________________________________________________
This file was modified from: https://github.com/aehrc/fhir-claml
*/

package au.csiro.fhir.claml;


import au.csiro.fhir.claml.Claml2FhirResult.Concept.ConceptProperty.PropertyValueType;
import au.csiro.fhir.claml.model.claml.Class;
import au.csiro.fhir.claml.model.claml.*;
import ca.uhn.fhir.parser.DataFormatException;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.*;


@SuppressWarnings("HttpUrlsUsage")
public class FhirClamlConceptService {

    protected static final Logger log = LoggerFactory.getLogger(FhirClamlConceptService.class);

    protected final File clamlFile;
    protected final List<String> displayRubrics;
    protected final String definitionRubric;
    protected final List<String> exludeRubricProperties;
    protected final Map<String, String> metaToPropertyMap;
    protected List<String> designationRubrics;
    protected List<String> excludeClassKind;
    protected final Boolean excludeKindlessClasses;
    protected final Boolean applyModifiers;

    public FhirClamlConceptService(
            File clamlFile,
            List<String> displayRubrics,
            String definitionRubric,
            List<String> designationRubrics,
            List<String> excludeClassKind,
            List<String> excludeRubricProperties,
            Map<String, String> metaToPropertyMap,
            Boolean excludeKindlessClasses,
            Boolean applyModifiers
    ) {
        this.clamlFile = clamlFile;
        this.displayRubrics = displayRubrics;
        this.definitionRubric = definitionRubric;
        this.designationRubrics = designationRubrics;
        this.excludeClassKind = excludeClassKind;
        this.exludeRubricProperties = excludeRubricProperties;
        this.excludeKindlessClasses = excludeKindlessClasses;
        this.metaToPropertyMap = metaToPropertyMap;
        this.applyModifiers = applyModifiers;
    }

    protected ClaML parseFileAsClaml(File clamlFile) throws SAXException, ParserConfigurationException, JAXBException, FileNotFoundException {

        JAXBContext jaxbContext = JAXBContext.newInstance(ClaML.class);

        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        spf.setFeature("http://xml.org/sax/features/validation", false);

        XMLReader xmlReader = spf.newSAXParser().getXMLReader();
        InputSource inputSource = new InputSource(new FileReader(clamlFile));
        SAXSource source = new SAXSource(xmlReader, inputSource);

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        return (ClaML) jaxbUnmarshaller.unmarshal(source);
    }

    public Claml2FhirResult babelfshClaml2Fhir() throws DataFormatException, IOException, ParserConfigurationException, SAXException, JAXBException {

        if (designationRubrics == null) {
            designationRubrics = Collections.emptyList();
        }
        if (excludeClassKind == null) {
            excludeClassKind = Collections.emptyList();
        }

        log.debug("Parsing ClaML file " + clamlFile.getAbsolutePath());
        ClaML claml = parseFileAsClaml(clamlFile);
        log.debug("Converting ClaML to FHIR");
        Claml2FhirResult claml2FhirResult = claml2FhirObject(claml, displayRubrics, definitionRubric, designationRubrics, excludeClassKind,
                excludeKindlessClasses, applyModifiers);
        log.debug("Conversion complete");
        return claml2FhirResult;
    }

    protected Claml2FhirResult claml2FhirObject(ClaML claml,
                                                List<String> displayRubrics,
                                                String definitionRubric,
                                                List<String> designationRubrics,
                                                List<String> excludeClassKind,
                                                Boolean excludeKindlessClasses,
                                                Boolean applyModifiers) {

        // Default values
        if (displayRubrics == null || displayRubrics.isEmpty()) {
            displayRubrics = new ArrayList<>();
            displayRubrics.add("preferred");
        }
        if (definitionRubric == null) {
            definitionRubric = "definition";
        }

        LinkedList<Claml2FhirResult.CsProperty> csProperties = new LinkedList<>();

        for (RubricKind rk : claml.getRubricKinds().getRubricKind()) {

            if (!definitionRubric.equals(rk.getName()) &&
                    !displayRubrics.contains(rk.getName()) &&
                    !designationRubrics.contains(rk.getName())) {


                if (this.exludeRubricProperties.contains(rk.getName())) {
                    log.info("Excluding rubric kind '%s' from properties".formatted(rk.getName()));
                    continue;
                }
                if (rk.getDisplay() != null && !rk.getDisplay().isEmpty()) {
                    if (rk.getDisplay().size() > 1) {
                        log.warn("Found more than one display for rubric kind " + rk.getName() + ": ignoring additional displays");
                    }
                    csProperties.add(new Claml2FhirResult.CsProperty(rk.getName(), rk.getDisplay().getFirst().getContent()));

                } else {
                    csProperties.add(new Claml2FhirResult.CsProperty(rk.getName(), null));
                }
            }
            if (rk.isInherited()) {
                log.warn("Inherited rubric kinds are not fully supported: " + rk.getName());
            }
        }

        log.debug("Built rubrics: {}", csProperties);
        int count = 0;

        Map<String, Claml2FhirResult.Concept> concepts = new HashMap<>();
        Map<String, List<ModifiedBy>> modifiedBy = new HashMap<>();
        Map<String, Set<ExcludeModifier>> excludeModifiers = new HashMap<>();
        Map<String, Set<String>> descendents = new HashMap<>();

        log.debug("Processing {} classes", claml.getClazz().size());

        for (Class c : claml.getClazz()) {
            count += produceConceptFromClass(excludeClassKind, excludeKindlessClasses, c, concepts, modifiedBy, descendents, excludeModifiers);
        }

        log.info("Processed {} classes, generating displays", count);
        concepts.forEach((code, concept) -> setDisplayForConcept(concept, concepts));

        Map<String, Set<ModifierClass>> modifierClasses = new HashMap<>();
        for (ModifierClass modClass : claml.getModifierClass()) {
            if (!modifierClasses.containsKey(modClass.getModifier())) {
                modifierClasses.put(modClass.getModifier(), new HashSet<>());
            }
            modifierClasses.get(modClass.getModifier()).add(modClass);
        }

        if (applyModifiers) {
            for (String modifiedConcept : modifiedBy.keySet().stream().sorted().toList()) {
                //Don't add modifiers to non-leaf classes
                if (descendents.containsKey(modifiedConcept) && !modifiedBy.get(modifiedConcept).isEmpty() && !descendents.get(modifiedConcept).isEmpty()) {
                    if (log.isInfoEnabled()) {
                        log.info("Modifiers are only applied to leaf classes. Skipping " + modifiedConcept);
                    }
                    for (String desc : descendents.get(modifiedConcept)) {
                        log.debug("Applying modifiers to descendent " + desc + " of code " + modifiedConcept);
                        applyModifiersToClass(desc, modifierClasses, modifiedBy.get(modifiedConcept), concepts, displayRubrics, excludeModifiers);
                    }
                } else {
                    count += applyModifiersToClass(modifiedConcept, modifierClasses, modifiedBy.get(modifiedConcept), concepts, displayRubrics, excludeModifiers);
                }
            }
        }

        log.info("After modifiers, processed {} classes", count);

        return new Claml2FhirResult(
                claml.getTitle().getContent(),
                csProperties,
                concepts.values().stream().toList()
        );
    }

    protected String generateDisplayForConceptFromLabel(Claml2FhirResult.Concept concept, Label label, Map<String, Claml2FhirResult.Concept> concepts) {
        return getLabelValue(label, true);
    }

    protected void setDisplayForConcept(Claml2FhirResult.Concept concept, Map<String, Claml2FhirResult.Concept> concepts) {
        Pair<List<Label>, String> preferredRubric = concept.getPreferredRubric(displayRubrics);
        if (preferredRubric != null && !preferredRubric.getLeft().isEmpty()) {
            if (preferredRubric.getLeft().size() > 1) {
                log.warn("Found more than one preferred rubric for code " + concept.getCode());
            }
            String display = generateDisplayForConceptFromLabel(concept, preferredRubric.getLeft().getFirst(), concepts);
            concept.setDisplay(display);
        }
        ArrayList<Label> definitionRubrics = concept.getSingleRubric(this.definitionRubric);
        if (definitionRubrics != null && !definitionRubrics.isEmpty()) {
            if (definitionRubrics.size() > 1) {
                log.warn("Found more than one definition rubric for code " + concept.getCode());
            }
            String definition = getLabelValue(definitionRubrics.getFirst(), false);
            concept.setDefinition(definition);
        }
        for (String dr : designationRubrics) {
            List<Label> designationRubrics = concept.getSingleRubric(dr);
            if (designationRubrics != null && !designationRubrics.isEmpty()) {
                for (Label l : designationRubrics) {
                    addDesignationForLabel(concept, dr, l);
                }
            }
        }
        List<String> unexpectedRubricKinds = concept.getRubrics().keySet().stream().filter(
                rk -> !displayRubrics.contains(rk) && !rk.equals(definitionRubric) && !designationRubrics.contains(rk)
        ).toList();
        unexpectedRubricKinds.forEach(rubric -> {
            // dump unexpected rubrics as designations
            ArrayList<Label> singleRubric = concept.getSingleRubric(rubric);
            singleRubric.forEach(s -> concept.addDesignation(getLabelValue(s, false), null, rubric));
        });


        if (concept.noDisplay()) {
            log.warn("Concept " + concept.getCode() + " has no display text. Using code as display text");
            concept.setDisplay(concept.getCode());
            if (concept.noDefinition()) {
                concept.setDefinition(concept.getCode());
            }
        } else if (concept.noDefinition()) {
            concept.setDefinition(concept.getDisplay());
            //TODO definitions?!
        }
/*

        Map<String, List<Rubric>> displayRubricValues = new HashMap<>();
        for (Rubric rubric : concept.getRubric()) {
            Object kind = rubric.getKind();
            if (kind instanceof RubricKind rkind) {
                if (this.exludeRubricProperties.contains(((RubricKind) kind).getName())) {
                    continue;
                }
                if (displayRubrics.contains(rkind.getName())) {
                    addDisplayRubric(displayRubricValues, rubric, rkind, concept.getCode());
                } else if (rkind.getName().equals(definitionRubric)) {
                    final String value;
                    if (rubric.getLabel().size() > 1) {
                        log.warn("Found more than one label on definition rubric for code " + concept.getCode());
                    }
                    value = getLabelValue(rubric.getLabel().getFirst(), false).trim();
                    concept.setDefinition(value);

                } else if (designationRubrics.contains(rkind.getName())) {
                    addDesignationsForRubric(concept, rubric);
                } else {
                    for (Label l : rubric.getLabel()) {
                        String v = getLabelValue(l, false).trim();
                        if (!v.isEmpty()) {
                            concept.addProperty(rkind.getName(), v, PropertyValueType.STRING);
                        }
                    }
                }
            } else {
                log.warn("Unexpected rubric kind " + kind);
            }
        }

        for (String dr : displayRubrics) {
            if (!displayRubricValues.containsKey(dr)) {
                continue;
            }
            List<Rubric> values = displayRubricValues.get(dr);
            if (concept.noDisplay()) {
                if (values.size() > 1) {
                    log.warn("Found multiple display rubrics " + dr + " for code " + concept.getCode());
                }
                Rubric rubric = values.getFirst();
                RubricKind rubricKind = (RubricKind) rubric.getKind();
                String conceptDisplay = generateDisplayForUnmodifiedConcept(concept, rubric);
                concept.setDisplay(conceptDisplay, rubricKind.getName());

                if (concept.noDefinition()) {
                    concept.setDefinition(getLabelValue(rubric.getLabel().getFirst(), false).trim());
                }

                if (rubric.getLabel().size() > 1) {
                    if (log.isWarnEnabled()) {
                        log.warn("Found more than one label on display rubric " + dr + " for code " + concept.getCode());
                    }
                    for (int i = 1; i < values.size(); i++) {
                        addDesignationForLabel(concept, rubric, (RubricKind) rubric.getKind(), rubric.getLabel().get(i));
                    }
                }
            } else {
                // We've already got a display, dump everything else as a designation
                for (Rubric r : values) {
                    addDesignationsForRubric(concept, r);
                }
            }
        }
        if (concept.noDisplay()) {
            log.warn("Concept " + concept.getCode() + " has no display text. Using code as display text");
            concept.setDisplay(concept.getCode(), null);
            if (concept.noDefinition()) {
                concept.setDefinition(concept.getCode());
            }
        } else if (concept.noDefinition()) {
            concept.setDefinition(concept.getDisplay());
        }
*/

    }

    @NotNull
    private Integer produceConceptFromClass(List<String> excludeClassKind, Boolean excludeKindlessClasses, Class c, Map<String, Claml2FhirResult.Concept> concepts, Map<String, List<ModifiedBy>> modifiedBy, Map<String, Set<String>> descendents, Map<String, Set<ExcludeModifier>> excludeModifiers) {
        if (c.getKind() != null && excludeClassKind.contains(getClassKindName(c.getKind()))) {
            log.info("Concept " + c.getCode() + " has excluded kind " + getClassKindName(c.getKind()) + ": skipping");
            return 0;
        }
        if (concepts.containsKey(c.getCode())) {
            log.error("A concept already exists with code " + c);
        }

        Claml2FhirResult.Concept concept = new Claml2FhirResult.Concept(c.getCode());
        concepts.put(c.getCode(), concept);
        modifiedBy.put(c.getCode(), new ArrayList<>());
        if (!descendents.containsKey(c.getCode())) {
            descendents.put(c.getCode(), new HashSet<>());
        }
        if (c.getKind() != null) {
            if (getClassKindName(c.getKind()) != null) {
                concept.addProperty("kind", getClassKindName(c.getKind()), PropertyValueType.CODE);
            } else {
                log.warn("Unrecognised class kind on class " + c.getCode() + ": " + c.getKind());
            }
        } else if (excludeKindlessClasses) {
            log.info("Concept " + c.getCode() + " has excluded kind " + getClassKindName(c.getKind()) + ": skipping");
            return 0;
        } else {
            log.info("Concept " + c.getCode() + " has no kind.");
        }
        for (SubClass sub : c.getSubClass()) {
            concept.addProperty("child", sub.getCode(), PropertyValueType.CODE);
            descendents.get(c.getCode()).add(sub.getCode());
            if (descendents.containsKey(sub.getCode()) && !descendents.get(sub.getCode()).isEmpty()) {
                descendents.get(c.getCode()).addAll(descendents.get(sub.getCode()));
            }
        }
        for (SuperClass sup : c.getSuperClass()) {
            concept.addProperty("parent", sup.getCode(), PropertyValueType.CODE);
            if (!descendents.containsKey(sup.getCode())) {
                descendents.put(sup.getCode(), new HashSet<>());
            }
            descendents.get(sup.getCode()).add(c.getCode());
            if (!descendents.get(c.getCode()).isEmpty()) {
                descendents.get(sup.getCode()).addAll(descendents.get(c.getCode()));
            }
        }
        for (Rubric rubric : c.getRubric()) {
            Object rubricKind = rubric.getKind();
            if (rubricKind instanceof RubricKind kind) {
                if (exludeRubricProperties.contains(kind.getName())) {
                    continue;
                }
                if (rubric.getLabel().size() > 1) {
                    log.warn("Found more than one label on rubric " + kind.getName() + " for code " + c.getCode());
                    continue;
                }
                concept.addRubric(kind.getName(), rubric.getLabel().getFirst());
            }
        }

        if (!c.getModifiedBy().isEmpty()) {
            if (!modifiedBy.containsKey(c.getCode())) {
                modifiedBy.put(c.getCode(), new ArrayList<>());
            }
            if (log.isDebugEnabled()) {
                log.debug("Adding " + c.getModifiedBy().size() + " modifiers to class " + c.getCode());
            }
            modifiedBy.get(c.getCode()).addAll(c.getModifiedBy());
        }
        if (!c.getExcludeModifier().isEmpty()) {
            if (!excludeModifiers.containsKey(c.getCode())) {
                excludeModifiers.put(c.getCode(), new HashSet<>());
            }
            if (log.isDebugEnabled()) {
                log.debug("Adding " + c.getExcludeModifier().size() + " modifier exclusions to class " + c.getCode());
            }
            excludeModifiers.get(c.getCode()).addAll(c.getExcludeModifier());
        }

        if (!c.getMeta().isEmpty()) {
            for (Meta meta : c.getMeta()) {
                if (metaToPropertyMap.containsKey(meta.getName())) {
                    String mappedCode = metaToPropertyMap.get(meta.getName());
                    concept.addProperty(mappedCode, meta.getValue(), PropertyValueType.STRING);
                }
            }
        }

        return 1;
    }

    private void addDisplayRubric(Map<String, List<Rubric>> displayRubricValues,
                                  Rubric rubric,
                                  RubricKind rkind,
                                  String code) {
        if (rubric.getLabel().size() > 1) {
            log.warn("Found more than one label on display rubric " + rkind.getName() + " for code " + code);
        }
        if (!displayRubricValues.containsKey(rkind.getName())) {
            displayRubricValues.put(rkind.getName(), new ArrayList<>());
        }
        displayRubricValues.get(rkind.getName()).add(rubric);
    }

    @SuppressWarnings("UnnecessaryLabelOnContinueStatement")
    private int applyModifiersToClass(String modifiedConcept, Map<String, Set<ModifierClass>> modifierClasses,
                                      List<ModifiedBy> modifiedBy, Map<String, Claml2FhirResult.Concept> concepts,
                                      List<String> displayRubrics, Map<String, Set<ExcludeModifier>> excludeModifiers) {
        List<Claml2FhirResult.Concept> candidates = new ArrayList<>();
        int count = 0;
        candidates.add(concepts.get(modifiedConcept));
        //Apply the modifiers in order to the modified concept
        List<Claml2FhirResult.Concept> newCandidates;

        modifiers:
        for (ModifiedBy modBy : modifiedBy) {

            if (excludeModifiers.containsKey(modifiedConcept) && !excludeModifiers.get(modifiedConcept).isEmpty()) {
                for (ExcludeModifier excludeMod : excludeModifiers.get(modifiedConcept)) {
                    if (modBy.getCode().equals(excludeMod.getCode())) {
                        log.info("Modifier " + modBy.getCode() + " is excluded for class " + modifiedConcept + " : Skipping");
                        continue modifiers;
                    }
                }
            }

            // for each candidate
            newCandidates = new ArrayList<>();
            for (Claml2FhirResult.Concept cand : candidates) {
                Set<ModifierClass> affectedModifierClasses = modifierClasses.get(modBy.getCode());
                log.debug("Applying modifier " + modBy.getCode() + " with " + affectedModifierClasses.size() + " modifierClasses to " + cand.getCode());

                // for each applicable ModifierClass
                modifierClasses:
                for (ModifierClass modClass : affectedModifierClasses) {
                    log.debug("Applying modifierClass " + modClass.getCode() + " to " + cand.getCode());

                    // If ModifiedBy.all == false and there is no ModifiedBy.ValidModifierClass for this modifier class, then skip it
                    if (!modBy.isAll() && modBy.getValidModifierClass().stream().noneMatch(vmc -> vmc.getCode().equals(modClass.getCode()))) {
                        log.info("Skipping modifierClass " + modClass.getCode() + " due to missing ValidModifierClass on class " + cand.getCode());

                        continue modifierClasses;
                    }
                    for (Meta excl : modClass.getMeta().stream().filter(met -> met.getName().equals("excludeOnPrecedingModifier")).toList()) {
                        String[] substrings = excl.getValue().split(" ");
                        if (substrings.length == 2 && cand.getCode().endsWith(substrings[1])) {
                            log.info("Skipping modifierClass " + modClass.getCode() + " due to excludeOnPrecedingModifier on class " + cand.getCode());
                            continue modifierClasses;
                        }
                    }
                    String newCode = cand.getCode() + modClass.getCode();
                    if (concepts.containsKey(newCode)) {
                        log.warn("Code " + newCode + " already exists as a declared Class - skipping application of modifierClass " + modBy.getCode() + "::" + modClass.getCode() + " to code " + cand.getCode());
                        continue modifierClasses;
                    }

                    //Set code to append modifierClass code
                    Claml2FhirResult.Concept concept = new Claml2FhirResult.Concept(newCode);
                    count++;
                    newCandidates.add(concept);
                    log.debug("Creating code " + concept.getCode());
                    Map<String, List<Rubric>> displayRubricValues = new HashMap<>();
                    //Fix display to append modifierClass display
                    for (Rubric rubric : modClass.getRubric()) {
                        Object kind = rubric.getKind();
                        if (kind instanceof RubricKind rkind) {
                            if (displayRubrics.contains(rkind.getName())) {
                                String superClassCode = modClass.getSuperClass().getCode();
                                if (superClassCode != null && !superClassCode.startsWith("ST")) {
                                    List<ModifierClass> list = affectedModifierClasses.stream().filter(c -> c.getModifier().equals(modClass.getModifier()) && c.getCode().equals(superClassCode)).toList();
                                    list.stream().findFirst().ifPresent(
                                            superClass -> {
                                                for (Rubric superClassRubric : superClass.getRubric()) {
                                                    RubricKind superClassRubricKind = (RubricKind) superClassRubric.getKind();
                                                    addDisplayRubric(displayRubricValues, superClassRubric, superClassRubricKind, superClassCode);
                                                    concept.addRubric(superClassRubricKind.getName(), superClassRubric.getLabel().getFirst());
                                                }
                                            }
                                    );
                                }
                                addDisplayRubric(displayRubricValues, rubric, rkind, modClass.getCode());
                                concept.addRubric(rkind.getName(), rubric.getLabel().getFirst());
                                //TODO!
                            }
                        }
                    }
                    for (String dr : displayRubrics) {
                        if (!displayRubricValues.containsKey(dr)) {
                            continue;
                        }
                        List<Rubric> values = displayRubricValues.get(dr);
                        String value = getLabelValueForMultipleRubrics(values, dr, modClass, modifiedConcept);
                        String newDisplay = generateDisplayForModifiedConcept(cand, concepts.get(modifiedConcept), value, modClass, concepts);
                        concept.setDisplay(newDisplay);
                        break;
                    }
                    // we consider the property 'kind' as transitive, since it is required on the Class element in ClaML
                    Optional<Claml2FhirResult.Concept.ConceptProperty> kindProperty = cand.getProperties().stream().filter(p -> p.code().equals("kind")).findFirst();
                    kindProperty.ifPresentOrElse(p -> concept.addProperty("kind", p.valueAsString(), p.valueType()), () -> log.warn("No kind property found on " + cand.getCode()));

                    // Remove old parent/child links
                    //concept.getProperty().removeIf(p -> p.getCode().equals("parent") || p.getCode().equals("child"));
                    concept.addProperty("parent", cand.getCode(), PropertyValueType.CODE);
                }
            }
            candidates = newCandidates;
            if (!candidates.isEmpty()) {
                for (Claml2FhirResult.Concept c : candidates) {
                    concepts.putIfAbsent(c.getCode(), c);
                }
            }
        }
        return count;
    }

    protected String getLabelValueForMultipleRubrics(List<Rubric> values, String dr, ModifierClass modClass, String modifiedConcept) {
        if (values.size() > 1) {
            log.warn("Found multiple display rubrics " + dr + " for modifierClass " + modClass.getCode() + "on code " + modifiedConcept);
        }
        Rubric rubric = values.getFirst();
        if (rubric.getLabel().size() > 1) {
            log.warn("Found more than one label on display rubric " + dr + " for code " + modClass.getCode());
        }
        return getLabelValue(rubric.getLabel().getFirst(), true).trim();
    }

    protected String generateDisplayForModifiedConcept(
            Claml2FhirResult.Concept candidate,
            Claml2FhirResult.Concept modifiedConcept,
            String displayRubricValue,
            ModifierClass modClass,
            Map<String, Claml2FhirResult.Concept> concepts) {
        return String.format("%s : %s", candidate.getDisplay(), displayRubricValue);
    }

    private void addDesignationForLabel(Claml2FhirResult.Concept concept, String designationUse, Label l) {
        if (getLabelValue(l, false) == null) {
            log.warn("Skipping null label for designation {} for concept {}", getLabelValue(l, false), concept.getCode());
            return;
        }
        String v = getLabelValue(l, false).trim();
        if (!v.isEmpty()) {
            if (!concept.noDisplay() && concept.getDisplay().equals(v)) {
                return;
            }
            concept.addDesignation(v, l.getLang(), designationUse);
        } else {
            log.warn("Skipping empty label for rubric {} for concept {}", getLabelValue(l, false), concept.getCode());
        }
    }

    private String getClassKindName(Object kind) {
        if (kind instanceof String) {
            return (String) kind;
        } else if (kind instanceof ClassKind) {
            return ((ClassKind) kind).getName();
        } else {
            log.warn("Unrecognized class kind:" + kind);
            return null;
        }
    }

    protected String getLabelValue(Label label, boolean forDisplay) {
        StringBuilder result = new StringBuilder();
        for (Object cont : label.getContent()) {
            if (!result.isEmpty()) {
                result.append("\n");
            }
            result.append(getLabelValue(cont, forDisplay));
        }
        return result.toString();
    }

    protected String getLabelValue(Reference reference, boolean forDisplay) {
        return "[" + reference.getContent() + "]";
    }

    protected String getLabelValue(Fragment fragment, boolean forDisplay) {
        StringBuilder result = new StringBuilder();
        for (Object cont : fragment.getContent()) {
            if (fragment.getType() != null && fragment.getType().equals("list")) {
                result.append(" - ");
            }
            result.append(getLabelValue(cont, forDisplay));
        }
        return result.toString();
    }

    protected String getLabelValue(Term term, boolean forDisplay) {
        switch (term.getClazz()) {
            case "tab" -> {
                return "\t";
            }
            case "subscript" -> {
                return "_" + term.getContent();
            }
            case "italics", "bold" -> {
                return term.getContent();
            }
            default -> {
                log.warn("Unrecognized Term class:" + term.getClazz());
                return term.getContent();
            }
        }
    }

    protected String getLabelValue(Object l, boolean forDisplay) {
        switch (l) {
            case Label label -> {
                return getLabelValue(label, forDisplay);
            }
            case String s -> {
                return s;  // This is a hack, check all contents?
            }
            case Reference reference -> {
                return getLabelValue(reference, forDisplay);
            }
            case Para para -> {
                StringBuilder result = new StringBuilder();
                for (Object cont : para.getContent()) {
                    result.append(getLabelValue(cont, forDisplay));
                }
                return result.toString();
            }
            case Fragment frg -> {
                return getLabelValue(frg, forDisplay);
            }
            case Term term -> {
                return getLabelValue(term, forDisplay);
            }
            case au.csiro.fhir.claml.model.claml.List list -> {
                StringBuilder result = new StringBuilder();
                for (ListItem item : list.getListItem()) {
                    result.append(" - ");
                    for (Object cont : item.getContent()) {
                        result.append(getLabelValue(cont, forDisplay));
                    }
                    result.append("\n");
                }
                return result.toString();
            }
            case null, default -> {
                assert l != null;
                log.warn("Ignoring non-String label contents on Label (" + l.getClass().getSimpleName() + ")");
                return l.getClass().getSimpleName().toUpperCase();
            }
        }
    }

}
